---
title: Utilisation du thème
disableToc: true
slug: utilisation
---
## Installation du site

Ce site est principalement écrit en Markdown, converti en html par le [générateur de site statique](https://www.staticgen.com/) Hugo.

### Installation

Pour installer le site et son thème, voici les étapes à suivre :

- Installer [Hugo](https://gohugo.io/) sur votre machine
- Cloner ou Forker le dépot git du site en local :

```bash
git clone https://gitlab.com/giloop-projects/cours-giloop.git mes-cours
cd mes-cours
```

### Configuration

Les paramètres du site sont dans le fichier `config.toml` à la racine du dossier. Adaptez-les selon vos besoins. 

### Publication

Pour publier le site en ligne avec l'intégration continue de Gitlab, il faut modifier le fichier `.gitlab-ci.yml`  en indiquant le chemin pour publication de la page :

```yml
pages:
  script:
    - hugo --baseURL https://giloop-projects.gitlab.io/cours-giloop/
```

## Fresh installation

Si vous êtes juste intéressé par le modèle de site, vous pouvez faire une installation depuis zéro en reprenant le thème d'origine, suivez les étapes suivantes :

```bash
# Création d'un nouveau site avec hugo
hugo new site mes-cours
# On se place dans le dossier du site
cd mes-cours
# Initialisation du dépot git (le dossier public n'est pas dans le suivi)
echo "public" >> .gitignore
git init
# Récupération du thème Learn
git submodule add https://github.com/matcornic/hugo-theme-learn.git themes/learn
# Commit
git commit -m "Initial commit"
# Ajout du dépot distant et envoi des données
git remote remove origin
git remote add origin https://gitlab.com/giloop-projects/cours-giloop.git
git push -u origin master
```

Dans ce cas, pour lancer le site il est nécessaire de d'abord recopier le contenu du dossier ``themes/learn/exampleSite`` à la racine du site et de paramétrer le fichier `config.toml` avec votre configuration. Pour ce site (en français uniquement), j'utilise :

```toml
baseURL = "/"
languageCode = "fr-FR"
defaultContentLanguage = "fr"

title = "Cours le l'IMTS"
theme = "learn"
themesdir = "themes"
metaDataFormat = "yaml"

defaultContentLanguageInSubdir= false

[params]
  editURL = "https://giloop-projects.gitlab.io/cours-giloop/"
  description = "Support de cours IMTS"
  author = "Gilles Gonon"
  showVisitedLinks = true
  disableBreadcrumb = false
  disableNextPrev = false
  # When using mulitlingual website, disable the switch language button.
  disableLanguageSwitchingButton = true

# Activation de l'indexation / recherche
[outputs]
home = [ "HTML", "RSS", "JSON"]

[Languages]
[Languages.fr]
title = "Support de cours IMTS"
weight = 2
languageName = "Français"
```

## Quelques infos en vrac

Il n'est normalement pas nécessaire de modifier les fichiers du dossier thème [Hugo Learn](https://github.com/matcornic/hugo-theme-learn/).

Pour modifier les mises en page, on **surcharge** plutôt les fichiers du thème en les plaçant dans le dossier ``layouts/partials``. Le logo s'y trouve, ainsi que le header pour modifier le titre des pages.

Pour modifier le css : fichier ``static/css/theme-mine.css``.
