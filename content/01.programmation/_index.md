---
title: "Programmation"
weight: 1
# pre: "<b>1. </b>"
chapter: true
---

### Partie 1

# Programmation

Les notions essientielles de programmation, présentées pour les langages [Python](https://python.org) et Javascript.

## C'est quoi un programme ?

Une suite d'instruction envoyées au processeur pour qu'il les exécute (CPU, microcontrôleur, ...).  Le processeur ne comprend que des **instructions machines**, qui sont assez illisibles et rébarbatives.
Le programme est aussi appelé **code source**.
Une multitude de langages existe (plusieurs centaines) , Les différents langages présentent souvent des intérêts dans des domaines particuliers :

- Python : initialement un langage de scripting, qui peut tout faire aujourd'hui (data, web, bdd, gui, ...)
- Javascript : initialement un langage pour les applications web, qui peut quasiment tout faire aujourd'hui
- Java : applications d'entreprise (avec une bonne gestion des erreurs)
- C++ : applications avec besoin de rapidité
- PHP : processeur de HTML
- ...
![os-langages.png](/cours-giloop/images/os-langages.png)


## Les paradigmes de programmation

Un paradigme de programmation est l'approche que l'on a de la programmation informatique pour traiter un problème et le formuler dans un langage.  Différents paradigme sont par exemple :

- Impératif (ou procédural) : le programme est composé d'une suite d'instructions dont on compose le flux (séquençage). Ex. de langages : C, fortran, COBOL
- Déclaratif fonctionnel (ou logique) : le programme décrit des fonctions (ou prédicats) qui peuvent être appelé récursivement. Ex. de langages : Lisp, Haskell, oCaml
- Orienté objet : les programmes sont découpés en modules isolés les uns des autres, les objets, qui communiquent par message et conservent leur données et les méthodes associées (état). Ex. de langages : C++, Java,  Pharo (Smalltalk)
- Concurrent : le programme peut effectuer plusieurs tâches en mêmes temps. Ex. de langages :
- Visuel : le programme n'est plus un texte mais un dessin
- Événementiel : le programme ne suit pas une séquence mais réagi à des actions (événements)
- Basé Web : les images et le code s'échangent entre ordinateur. Ex. de langages : Java, PHP, Javascript

## Quelques principes

### La dette technique

À faire, en attendant on peut consulter [wikipedia](https://fr.wikipedia.org/wiki/Dette_technique).

### DRY "don't repeat yourself"

Ce principe est d'éviter au maximum les répétitions d'informations dans un programme. Chaque élément de connaissance ou de logique d'un programme doit avoir une représentation unique et non ambiguë.

L'erreur associée est le **WET** (We enjoy typing ou Wasting everyone's time) qui consiste à réécrire plusieurs les mêmes bouts de code à différents endroits, ce qui implique que la maintenance en sera compliquée car redondante .

La méthode consiste à diviser le système en petites unités réutilisables (fonctions) appelées. Il est rarement nécessaire d'écrire des fonctions/méthodes très longues.

Les bénéfices du DRY sont :

- l'écriture du code est plus rapide et moins douloureuse
- le maintenance est simplifiée
- le risque de bugs est moindre

### KISS : Keep it simple, stupid

Ce principe est de garder un code lisible, simple et clair, facile à comprendre. Si la machine ne comprend que des 0 et des 1, ce n'est pas le cas des humains. La programmation est faite aussi pour ceux qui la font.  
Un indicateur est d'avoir des fonctions qui ne dépassent pas 50 lignes (hors commentaires).

> Une fonction doit assurer 1 seule fonction, celle pourquoi elle est créée.

La méthode pour y arriver est d'envisager différentes méthodes pour résoudre un problème et de conserver la plus simple.

### SOLID

Traduction de la page de présentation des [pratiques de programmation](https://siderite.blogspot.com/2017/02/solid-principles-plus-dry-yagni-kiss-final.html) : SOLID, DRY, Yagni, Kiss ...

SOLID est un acronyme de :

- **S** : SRP single responsibility : chaque unité de code doit avoir une et une seule fonctionnalité (et un test unitaire associé). Chaque évolution est ainsi  
- **O** : OCP
- **L** : LSP
- **I** : ISP
- **D** : DIP

### Le Zen du Python

Recommandation [PEP 20 de Python](https://www.python.org/dev/peps/pep-0020/), traduite par Cécile Trevian et Bob Cordeau
> Préfère :
>
> - la beauté à la laideur,
> - l'explicite à l'implicite,
> - le simple au complexe
> - et le complexe au compliqué,
> - le déroulé à l'imbriqué,
> - l'aéré au compact.
> - Prends en compte la lisibilité.
> - Les cas particuliers ne le sont jamais assez pour violer les règles.
> - Mais, à la pureté, privilégie l'aspect pratique.
> - Ne passe pas les erreurs sous silence,
> - ... ou bâillonne-les explicitement.
> - Face à l'ambiguïté, à deviner ne te laisse pas aller.
> - Sache qu'il ne devrait y avoir qu'une et une seule façon de procéder, même si, de prime abord, elle n'est pas évidente, à moins d'être Néerlandais.
> - Mieux vaut maintenant que jamais.
> - Cependant jamais est souvent mieux qu'immédiatement.
> - Si l'implémentation s'explique difficilement, c'est une mauvaise idée.
> - Si l'implémentation s'explique aisément, c'est peut-être une bonne idée.
> - Les espaces de nommage ! Sacrée bonne idée ! Faisons plus de trucs comme ça.
> Et dites vous bien que dans la vie, ne pas reconnaître son talents, c'est favoriser la réussite des médiocres.  
> _Michel Audiard, Le cave se rebiffe_

## FAQ

Quels sont les quatre principaux niveaux de programmation pour les langages Web ?

- Procéduraux,
- Programmation orientée objet (OOP),
- MVC : Modèle Vue Contrôleur,
- ORM : mapping objet-relationnel (une couche d'abstraction entre le monde objet et monde relationnel), composant disponibles dans les frameworks)

## Ressources

- [DevDocs](https://devdocs.io) : site centralisant les références de nombreux langages