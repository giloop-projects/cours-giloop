---
title: Programmation avancée
weight: 2
# pre: "<b>1. </b>"
chapter: false
---


Les notions avancées de programmation, comme la programmation objet, la conception d'application ou encore les designs patterns. Ce notions seront présentées pour les langages [Python](https://python.org) et Javascript.

{{% notice warning %}}
A compléter
{{% /notice %}}
