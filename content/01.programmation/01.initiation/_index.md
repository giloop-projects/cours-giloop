---
title: Initiation
weight: 1
chapter: false
tags: ["dev", "programmation", "initiation"]
---

## Les notions de base

Indépendamment du langage utilisé, on doit connaître les notions suivantes de programmation. Pour les différents langages on aura des syntaxes différentes, mais on retrouvera immanquablement ces notions.

1. les variables (booléen, entiers, réels, texte, ...)
2. les branchements : if/else
3. les boucles
4. les variables de types conteneurs (tableaux, listes, dictionnaires, ...)
5. les fonctions (ou procédures ou méthodes)
6. les entrées / sorties (sur fichier, BDD, ..., ou simplement print dans la console)

> Vous devez d'abord manipuler ces notions pour les maîtriser et vous pourrez ensuite passer très vite à la vitesse supérieure : objets, applications, interfaces, closures, ...


Les notions essientielles de programmation : *variables*, *structures principales* et *fonctions*, présentées pour les langages [Python](https://python.org) et Javascript.

{{% notice warning %}}
A compléter
{{% /notice %}}
