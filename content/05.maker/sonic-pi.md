---
title: "Sonic Pi : coder la musique"
author: [Gilles Gonon]
weight: 10
chapter: false
tags: ["music", "coding", "ruby", "musiccoding", "sonicpi", "creative"]
---

# Coder en musique avec Sonic Pi

[Sonic Pi](https://sonic-pi.net/) est un logiciel gratuit qui permet de coder de la musique.

Il est possible de l'installer pour Windows / Linux / mac OS.

Le logiciel inclut une documentation en français idéale pour démarrer. Elle est très complète et intégrée au logiciel, ce qui permet d'apprendre en s'amusant en recopiant les nombreux exemples disponibles. 

L'esprit du logiciel est :

> Libérez votre créativité  
> Apprenez en vous amusant  
> Profitez de vos erreurs pour créer des possibles musicaux

Quelques ressources sympas :

- Un dépôt avec des morceaux et des samples sympas : [Github Luiscript SonicPi](https://github.com/luiscript/SonicPi-Music.git)
- [Sonic Pi sur Mehackit](http://sonic-pi.mehackit.org/index_fr.html)
- PDF en anglais : [l'essentiel de Sonic Pi](https://raspberrypi.org/magpi-issues/Essentials_Sonic_Pi-v1.pdf)

## Complément d'information

### Jouer des notes de musique

![numéro midi, nom des notes, fréquences](/cours-giloop/images/numero-midi-nom-des-notes.png)

Notation anglaise des notes de musique
![notation-anglaise-notes.png](/cours-giloop/images/notation-anglaise-notes.png)

```ruby
# La :A Si :B Do :C Ré :D Mi :E
# Dièse s (sharp) - Bémol b

play :Ab4
sleep 1
play :A4
sleep 1
play :As4
sleep 1
play :B4
sleep 1
play :C5
sleep 1
play :Cs5
sleep 1
play :D5
sleep 1
```

## Quelques exemples

```ruby
# Au clair de la lune
# Création d'un tableau circulaire des notes
notes = (ring :C4, :C4, :C4, :D4, :E4, :D4,
         :C4, :E4, :D4, :D4, :C4)

# Création d'un tableau circulaire des durées
durees = (ring 1, 1, 1, 1, 2, 2,
          1, 1, 1, 1, 4)

live_loop :slow do
  # Choix de la vitesse en bpm: battement par minute
  use_bpm 100
  # tick renvoie un compteur d'itération de la boucle
  t = tick
  print t # affichage dans le journal à droite ->
  
  # On joue les notes
  play notes[t], release: durees[t]
  sleep durees[t]
end
```

```ruby
# Utilisation d'un sample en local (dans mon C:)
omg1 = "/Dev/SonicPi/samples/voice_omg_male.wav"
omg2 = "/Dev/SonicPi/samples/voice_omg_butt_female.wav"

live_loop :test do
  v_rate = rrand(0.5,2.0) # * choose([-1, 1])
  sample omg1, rate: v_rate
  print v_rate
  sleep  0.765*(sample_duration omg1, rate: v_rate)
end
```

```ruby
# Montée et descente de gamme
# Note midi : 60 = Do4 = C4

n_depart = 60  # note de départ
n_fin = 72     # note de fin
dir = 1        # direction
n = n_depart
live_loop :montee do
  use_bpm 200
  play n, attack: 0.025, release: 1
  if n<n_depart or n>n_fin
    dir = -dir
  end
  n = n + dir
  sleep 1
end
```

```ruby
# Petite boucle Gilles
use_bpm 80
live_loop :batterie do
  if one_in(2)
    sample :drum_snare_soft
  else
    sample :drum_bass_soft
  end
  sleep 0.5
end

live_loop :bass do
  use_synth :blade
  play choose([:E2, :G2, :A2, :E3]), release: 0.25, amp: rrand(0.5, 1.0)
  sleep choose([0.5, 0.25, 0.5])
end
```
