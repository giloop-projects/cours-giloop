---
title: "Faire des photobooth"
author: [Gilles Gonon]
weight: 12
chapter: false
tags: ["maker", "photo", "photobooth", "arduino", "processing"]
---

## 10 ans de Photobooths

Je réalise des photobooths depuis au moins 2010, à base de Raspberry Pi ou d'OpenFrameworks en utilisant des webcams ou un appareil piloté grace à la librarie [gphoto2](http://gphoto.org/). 

Vous trouverez une description et des exemples sur le site du [Gueulomaton](https://gueulomaton.org), notamment : 

- Le plus simple : un bouton photo pour déclencher l'appareil, les photos tournent en boucle sous forme de diaporama. Outils : Python + Gphoto + RaspberryPi + GPIO pour allumer des leds indiquant le statut de l'appareil. 
- Le plus fou : le gueulomaton, c'est votre cri qui déclenche la photo. OpenFrameworks + webcam + processing pour la diffusion/impression des photos sur une borne séparée
- Le plus freaky : les bouilles à facettes. Deux personnes s'installent devant une caméra et il en sort un gif animé du porphing entre les 2 visages. OpenFrameworks + webcam + processing
- Le plus dynamique : Le Sautomaton. "*1,2,3 Sautez*", une rafale de photos est prise et un réseau de neurones sélectionne la meilleure, là où vous êtes le plus haut. Python + Gphoto + RaspberryPi
- Le plus illuminé : l'Illuminaton, une cabine de LightPainting dont vous êtes le héro. Python + Gphoto + RaspberryPi

J'ajouterai (un jour) dans cette section des explications sur les codes utilisés pour créer ces photobooths. 

