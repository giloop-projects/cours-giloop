---
title: "Construction d'un polargraph"
author: [Gilles Gonon]
weight: 10
chapter: false
tags: ["maker", "drawbot", "polargraph", "3d"]
---

## Présentation

Depuis longtemps, j'ai une fascination pour les drawbot, ces robots qui dessinent. Les projets sur ce sujet son très nombreux : 

- Paul The Robot : une installation interactive qui vous tire le portrait de manière artistique (la machine à son propre style)
- Eggobt : une petite machine pour dessiner sur des oeufs
- Polargraph : dessins à grande échelle sur des feuilles
- Dessin dans le sable (sandtable par exemple)

## L'Eggbot

Il y a quelques années déjà, j'ai imprimé mon premier Eggbot et je me suis bien amusé avec, surtout durant la période de Pâques. 

{{% notice note %}}
Ajouter quelques photos
{{% /notice %}}

## Le polargraph

Il est maintenant temps de voir un peu plus grand. Dans ma maison actuelle, une ancienne étude de notaire, je dispose d'une vitrine me permettant d'afficher ce que je veux sur un espace de 1,65m*1m.

Notre idée était de ponctuer cet espace de messages à caractère poétique. Après un ou deux essais à la main plutôt moyen, je gardais en tête l'idée d'un drawbot qui pourraient nous aider à avoir de beaux résultats. 

C'est fait ! en partant d'une base du [Scriboo décrit par EmotionTech](https://www.reprap-france.com/article/realisez-vous-meme-un-drawbot-scriboo), j'ai modifié quelques pièces et je suis reparti de géniale version Processing du programme [controleur Polargraph](https://github.com/euphy/polargraphcontroller). Elle n'est pas très intuitive, mais après pas mal de tests, c'est super. En plus tout ça tourne sous un rapsberry pi ce qui permet d'avoir une installation "*portable*" pour faire des interventions de dessins grandeur nature. 

{{% notice note %}}
Ajouter quelques photos
{{% /notice %}}
