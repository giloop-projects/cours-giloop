---
title: Suivi de projet
weight: 45
#pre: "<b>2. </b>"
chapter: true
tags: [code, projet, gestion, suivi, version, git]
---

### Suivi de projet

# Planifier pour mieux (vous) développer

Que vous développiez seul ou en équipe, il est nécessaire d'avoir des outils pour planifier, suivre l'avancement de vos projets et gérer vos codes sources.
