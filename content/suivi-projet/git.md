---
title: "GIT pour le suivi de version"
author: [Gilles Gonon]
date: "2020-02-20"
weight: 2
tags: [programmation, git, code, version]
---

![icone-git.png](/cours-giloop/images/icone-git.png)

### Introduction à GIT

- Git est un logiciel de gestion de versions décentralisé.
- Logiciel libre créé par Linus Torvalds, auteur du noyau Linux
- Complexe mais indispensable

### Intérêt du suivi de version

- Suivre les évolutions de votre code, ne rien perdre
- Travailler en équipe
- Développer des nouvelles fonctionnalités sans tout casser
- Intégrer les fonctionnalités des autres

### Ressources en ligne

- Le [Pro Git Book](https://git-scm.com/book/fr/) : documentation complète 
- [Article Buzut sur Git](https://buzut.net/git-en-2-2/) : prise en main
- [Le petit guide de Git](http://rogerdudler.github.io/git-guide/index.fr.html) : les principales commandes par Roger Dudler (disponible en PDF)
- Aide sur [Gitlab](https://about.gitlab.com/) lors de la création d'un projet
- Cours [OpenClassroom](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git) (4h/difficile) : Il y a une bonne présentation de GITsur la page du cours
- Le [workflow proposé par Github](https://guides.github.com/introduction/flow/) doit être votre nouvelle référence
- Un [excellent article sur le Git workflow](https://nvie.com/posts/a-successful-git-branching-model/), qui a servi de modèle aux développeurs les 10 dernières années. Le workflow proposé peut cependant être simplifié (voir article précedent).

### Comment ça marche ?

- Principalement en ligne de commande !
- Intégration dans tous les IDE modernes

## Démarrer avec GitLab

![icone-gitlab.png](/cours-giloop/images/icone-gitlab.png)

### Quoi qu’est-ce ?

Serveur GIT fournissant des outils de développement :

- Hébergement du dépôt principal des sources d’un projet
- Suivi de version avec Git (commit, version, branches…)
- Intégration continue (tests automatiques, publication doc…)
- Revue de codes…

[Plus d'infos sur le site de Gitlab](https://about.gitlab.com/)

### Pour démarrer

- **Créer un compte GitLab** : https://gitlab.com/users/sign_in
- **Créer une clé ssh** : à faire sur votre PC
  - Ouvrir Git Bash sur son PC
  - Copier la commande suivante dans la console GIT (personnaliser l’adresse mail)
  
 ``$ ssh-keygen -t rsa -b 4096 -C "email@example.com"``

 La clef est générée par défaut dans un dossier .ssh de votre compte utilisateur par exemple :
 ``C:\Users\gille\.ssh``

### Enregistrer la clé SSH

- Se connecter à son compte GitLab et aller dans les Settings de son profil, puis choisir dans la barre de menu latérale :	 ``User Settings → SSH Keys``
- Éditer le fichier .pub (texte) du répertoire .ssh et copier tout son contenu dans le champ Key du navigateur GitLab, puis valider
- La clé est enregistrée
- Git peut se connecter au serveur

### HTTPS ou SSH ?

Pour se connecter à Gitlab et envoyer les codes sur le serveur, il y a 2 méthodes possibles :

- utiliser une clef SSH : il faut alors créer une paire de clef privée/publique
- identifiants du site GITLAB en connexion HTTPS : méthode la plus simple

Suivant la configuration du réseau, il est possible que seule une des méthodes fonctionne. A tester !

### Créer un nouveau projet

- GitLab >> Projects >> your projects
- Créer un nouveau projet
- Entrer le nom et la description du projet, Sélectionner la visibilité Private

### Gérer son projet

- Ouvrir le projet pour le configurer à Menu Settings
- Par défaut, le créateur du projet est désigné comme administrateur
- Il peut ensuite ajouter des invités ayant un compte utilisateur en allant dans
``Settings → Members``

### Cloner un dépot

Cloner un dépot consiste à suivre un dépot pour y apporter des modifications. Il faut disposer des droits d'accès, donnés par le propriétaire du dépot si ce n'est pas vous.

Attention il faut passer par le lien `SSH` ou `https` pour clôner le dépot.

- Il faut récupérer l’adresse SSH du dépôt:
  - Se connecter au serveur Gitlab
  - Ouvrir le projet concerné et récupérer l’adresse en cliquant sur « Clone » et bien recopier l’adresse « SSH »
- Ouvrir un terminal et se positionner dans le répertoire de travail
- Taper la commande ``git clone adresse_ssh_recupérée nom_repertoire_local``
  - ! Le nom du répertoire local n’est pas obligatoire. Il permet juste une meilleure organisation.
  - Par défaut un sous-répertoire du nom du dépot git est créé dans le dossier où l'on appelle la commande de clone.
- Si à la fin de la récupération de la branche distante, on n'a pas un dossier « .git » dans son répertoire local, il faut afficher les dossiers cachés
- On peut alors avec un clic droit pour utiliser soit ``Git Bash`` soit ``Git GUI`` pour les différentes actions possible avec Git.

## Forker un dépôt

Il est possible de récupérer un projet existant et de le copier comme un de ces projets pour démarrer son propre suivi.

## Le flux de travail

![git-workflow.png](/cours-giloop/images/git-workflow.png)

### Exemples avec Git bash

#### Commit du soir, espoir

Lorsque vous travailler en local, vous êtes sur une **branche** spécifique (par défaut master). En fin de journée il est bon de mettre à jour le suivi de version de son dépot en local, avec un ``add`` & ``commit``, puis de _pousser_ sur le serveur (gitlab, github, ...) avec un ``push``. Une bonne pratique est de faire d'abord un ``pull`` pour récupérer les modifications des autres.

```bash
git add .
git commit -m "ajout fonction xxx, correction de bugs, ..."
git pull
git push -u origin master
```

#### Résolution des conflits


Lors du ``pull`` il est fréquent de devoir gérer des **conflits** de fusion des codes si vous travaillez à plusieurs. Si un conflit apparaît, vous aurez le message suivant : 

```git
Removing Ressources/Ressources Python.md
Removing Formations/Développeur Logiciel/Ressources Web.md
Removing Formations/Développeur Logiciel/Questionnaire auto-évaluation.md
Auto-merging Formations/Développeur Logiciel/Cours BD relationnel.md
CONFLICT (content): Merge conflict in Formations/Développeur Logiciel/Cours BD relationnel.md
Automatic merge failed; fix conflicts and then commit the result.
```


## Publier une page sur Gitlab

La demande : Vous avez fait une page en local et vous voulez la publier en ligne sur Gitlab.

### Les étapes

- Créer un nouveau projet **vide** et **public** sur Gitlab
![nouveau-projet-gitlab.png](/cours-giloop/images/nouveau-projet-gitlab.png)
![projet-public.png](/cours-giloop/images/projet-public.png)

![git-clone-https.png](/cours-giloop/images/git-clone-https.png)

- Reprendre les explications de `push an existing folder`en changeant l'adresse **ssh** par l'adresse **https** du dépôt. Pour cela, il faut aller sur le bouton clone du dépôt et choisir **Copy https clone URL**. Cette option n'est accessible que lorsque l'écran est de petite taille.

```bash
git init
git remote add origin https://gitlab.com/gillou/cours-javascript.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

- Ouvrir GIT BASH dans le dossier où vous avez le code de la page web.
![ouvrir-gitbash.png](/cours-giloop/images/ouvrir-gitbash.png)
- Recopier les commandes suivantes ligne par ligne (celles indiquées dans GitLab) en vérifiant bien que la ligne `git remote add origin ...` contienne l'adresse https.

```bash
git init
git remote add origin https://gitlab.com/gillou/cours-javascript.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### En cas de problème

#### Problème d'identification

Si ça ne fonctionne pas en raison d'un problème d'identification :

```shell
remote: HTTP Basic: Access denied
fatal: Authentication failed for 'https://gitlab.com/gillou/cours-javascript.git/'
```

Il faut réinitialiser le mot de passe ou l'identifiant de Gitlab :

![gestionnaire-ident-windows.png](/cours-giloop/images/gestionnaire-ident-windows.png)

![modif-identi-gitlab-win.png](/cours-giloop/images/modif-identi-gitlab-win.png)

![ident-win-gitlab.png](/cours-giloop/images/ident-win-gitlab.png)

Lorsque ça fonctionne on a un message du type :

```bash
$ git push -u origin master
Enumerating objects: 33, done.
Counting objects: 100% (33/33), done.
Delta compression using up to 4 threads
Compressing objects: 100% (31/31), done.
Writing objects: 100% (33/33), 8.27 KiB | 498.00 KiB/s, done.
Total 33 (delta 9), reused 0 (delta 0)
To https://gitlab.com/gillou/cours-javascript.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

#### Changement du remote de ssh → https

Si vous avez rentré les lignes par défaut indiquées dans Gitlab, il faut modifier l'adresse du serveur (remote) :

```bash
git remote remove origin
git remote add origin https://gitlab.com/gillou/cours-javascript.git
```

### Ajout de l'intégration continue (CI/CD)

Dans le dépot GITLAB il faut ajouter un fichier qui génère le déploiement du site. Le bouton [Set up CI/CD] permet de faire ça et propose des templates. Choisissez le type de template `.gitlab-ci.yml` et le modèle associé à votre type de site web, typiquement HTML pour des pages que vous avez codé vous mêmes en html.

![gitlab-setup-ci.png](/cours-giloop/images/gitlab-setup-ci.png)

![setup-ci-yml.png](/cours-giloop/images/setup-ci-yml.png)

![template-yml.png](/cours-giloop/images/template-yml.png)

![git-pull-push.png](/cours-giloop/images/git-pull-push.png)

## Accès à la page

L'accès prend un peu de temps surtout lors de la première création, cela peut prendre jusqu'à 30 minutes, même après la validation d'un commit dans le menu CI/CD → Pipelines.

Pour les commits suivants, le site est mis à jour dès sa validation. Si vous ne voyez pas les changements, il arrive que votre navigateur ait enregistré la page dans le cache et ne rafraichisse pas tous les éléments. Dans ce cas il faut vider le cache ou tester sur un autre navigateur.
