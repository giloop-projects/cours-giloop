---
title: "Autres compétences"
weight: 3
# pre: "<b>1. </b>"
chapter: true
---

### Partie 4

# Les autres habiletés

Des conseils et techniques pour documenter votre code et écrire de bons rapports.
