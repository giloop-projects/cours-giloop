---
title: "Faire des présentations à l'oral"
author: [Gilles Gonon]
weight: 10
chapter: false
tags: ["présentation", "softskill", "oral"]
---

## Présentation du projet de stage

### Quelques informations

- Votre présentation doit durer 35 minutes max, dont 10 minutes de démonstration 
- Il faut repartir du rapport de stage
- Vous pouvez garder la structure du rapport pour la présentation
- La présentation est réalisée avec un support (Powerpoint ou autre)
- Vous aurez un rétro-projecteur à disposition

### Canevas du référentiel

Le référentiel, spécifie de présenter **un** projet (pas forcément le stage) à l’aide d’un support de présentation avec un canevas très proche du rapport de stage :

- Présentation de l’entreprise et/ou du service et contexte du projet (cahier des charges, environnement humain et technique)
- Conception et codage des composants front-end et des composants back-end
- Présentation des éléments les plus significatifs de l’interface de l’application
- Présentation du jeu d’essai de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues) et analyse des écarts éventuels
- Présentation d’un exemple de recherche effectuée à partir de site anglophone
- Synthèse et conclusion (satisfactions et difficultés rencontrées)

### Structure de la présentation

A partir des étapes indiquées dans le référentiel de certification, nous vous proposons le plan suivant pour votre présentation :

- **Introduction**
  - **Se présenter** : qui on est, pourquoi on est là
- **Contexte du stage**
  - Expliquer votre choix de stage
  - Présentation de l'entreprise
  - Environnement humain et technique : lieu, votre rôle
  - Expliquer le sujet du stage, les objectifs, vos tâches (cahier des charges) en allant à l’essentiel
- **Réalisations** : Description de vos activités pendant votre stage
  - Conception et codage : reprendre les étapes présentées dans votre rapport
  - Présentation des éléments les plus significatifs de l’interface de l’application
  - Présentation du jeu d’essai de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues) et analyse des écarts éventuels
  - Présentation d’un exemple de recherche effectuée à partir de site anglophone
- **Démonstration**
  - Aperçu global du site ou de l'application avec une navigation sur les pages principales
  - Donner un exemple d'action utilisateur (inscription, traitements, achat, don, ...)
  - Montrer le parcours "qui marche" mais aussi ce qui est prévu quand ça ne marche pas (erreur de saisie) pour montrer la robustesse du site. 
- **Conclusion**
  - Synthèse du travail réalisé
  - Compétences développées
  - Apport du stage : bilan, quel impact sur votre vie professionnelle
  - Suite de la formation, projet professionnel

### Quelques conseils pour la présentation

- On compte en général 1 minutes par slides : compte tenu des 10 minutes de démonstration, il faut donc préparer entre 25 et 30 slides. C'est un indicateur, à ajuster lors de vos répétitions.
- Rappeler les points du sommaire pour articuler la présentation et voir l'avancement
- Gérer son temps :
  - Avoir un timer, utiliser le mode présentateur des outils ou une montre.
  - **Répéter en vous chronométrant** pour savoir le temps pur chaque partie, pour avoir un repère
- Pour la démonstration préparer ce que vous voulez montrer à l'avance. Ouvrez les fenêtres avant le début de la présentation
- Ce n'est pas une course, il faut parler à un rythme normal, voir plus posé qu'une discussion.

## Atelier prise de parole

{{% notice info %}}
Notes prise durant l'atelier de Bernabé au Hackathon Océan 2019.  
Matériel : Un paperboard avec 3 feuilles.
{{% /notice %}}

### Du stress

Lorsque l'on est en situation de stress, on devient focus, ce qui aide pour être concentré sur le sujet à présenter, mais on peut aussi oublier des choses.

Quelques trucs pour éviter que le stress prenne le dessus :

- Ancrage (cerveau dans les pieds écartés à 10h10) : permet de stabiliser un stress. Il ne faut pas non plus figer la présentation
- Respiration : exercice ``respiration au carré``
  - Souffler 5sec
  - Temps de pause
  - Inspirer 5sec
  - Temps de pause
- Répéter
- Un blanc n'est pas grave
- Admettre que l'on partage avec le public

### Un exercice

- Inviter les participants sur scène (face public)
- Monter sur scène et dire bonjour devant les autres  
- En 1 mot tout se voit, il y a des choses qui sont communiqués et qui peuvent nous dépasser

### Quelques trucs

Pour convaincre, notre propre langage corporel va s'exprimer. Pour ça, il faut choisir déjà en nous notre posture, par exemple : sourire, convaincre, ouverture, ...

Le rythme est important : parler lentement à beaucoup d'intérêt :

- temps de réfléchir à ce que l'on doit dire
- Mettre de l'intonation
- Insister sur des points
- Permet aussi au public de mémoriser
- Les pauses sont très importantes

Ralentir aide dans tout, pour soi et pour le public aussi : intonation, mémorisation

Quelques exemples de Speechs : "Je vous ai compris", "I have a dream" : très lents, très impactants

### Structure du pitch

Le pitch est un peu théâtral, il faut intensifier le discours. C'est un **story telling**, il faut **répéter l'info** pour que l'auditeur mémorise :

- **Se présenter** : qui on est, domaine d'action, pourquoi on est là.
- **Problématique** : montrer la difficulté (ex : image d'un gars qui s'arrache les cheveux). "Ajourd'hui il y a des gens qui cherchent à résoudre se problème mais c'est pourri :) ". Si le problème est bien posé, le public imagine qu'il faut une solution. Insister sur votre intention.
- **Poser les bénéfices** de la solution :  Le public doit comprendre les bons points.
- **Démonstration** : méthodologie pour arriver au démonstrateur et démo.
- **Perspectives et impact** : aujourd'hui on en est là, mais demain on veut aller beaucoup plus loin. 
- **Call to action** pour terminer un pitch, l'appel à l'action, interpeler l'auditeur pour l'engager via la rélfexion. "Si vous souhaitez nous aider, télécharger l'appli. Maintenant c'est à vous de .... Je vous invite à réfléchir sur ce point précis. ".  
- **Répéter 1 info importante** (idéalement 3 fois): l'objet, ou le nom de l'application, toute info. Répétez exactement la même phrase, le même message.

### Les auditeurs et questions

Le jury est bienveillant.

![le jury est bienveillant](/cours-giloop/images/requin-nemo-doris.png)

Ses questions -que l'on espère pertinentes- ne sont pas des attaques.

Prendre les questions comme autant de pistes à continuer d'explorer.

Sur les questions compliquées, pièges, imprécises :

- Indiquer votre compréhension de la question et ne pas hésiter à demander à reformuler
- Il vaut mieux dire ``je ne sais pas`` que de s'enliser dans des explications vaseuses.

## Workshop - La parole en public

Vidéo Youtube : Workshop - La parole en public, l'éloquence n'est pas un don

Cette vidéo contient plein d'infos très intéressantes. Certaines informations sont d'ordre psychologiques un peu avancées. 
Je l'ai regardée 2 fois en entier, la 2è fois pour prendre des notes à la volée, ci-dessous.

{{< youtube id="4po9h1uYmh4" width="50%" >}}

### Pourquoi c'est difficile

Parce qu'on a peur, de quoi ?

- la peur de prendre la parole en public est la n°1 (avant la peur de la mort)
- être jugé
- Ne pas maîtriser, ne pas se sentir légitime : peur de son propre jugement
- Ne pas trouver ses mots, trous de mémoire (faire)
- J'ai peur que l'autre découvre en moi quelquechose que je cache en jouant des rôles : ce que je suis n'est pas ce que je montre (travailler la congruence)

Paradoxalement, nous sommes un être social et la peur est celle de ne pas être accepté dans le groupe. 

Plus on a peur d'une chose, plus on va la provoquer pour conforter notre peur.

### Phase de préparation 

Trouver son identité à soi, trop de technique tue la technique, être soi (exemple des politiques dans la maitrise qui ne les )

Quand on construit sa présentation répondre à **3 questions** : 

1. Quel est le message que je veux transmettre ?
2. Qu'est-ce que jeveux inciter l'auditeur à faire (call to action) ?
3. Quels sont les points essentiels que je veux qu'il retienne ?

### Attitude

- Sourire
- Avoir le visage ouvert
- Mettre une Énergie positive
- Congruence / authenticité : le discours oral doit être en concordance avec le discours non verbal du corps (plein d'exemples donnés dans la vidéo), ainsi qu'un lien vers un TED talk intéressant sur l'influence de la posture corporelle sur notre attitude (2min de Wonder Woman)

La vidéo donne quelques trucs pour se donner de l'énergie et diminuer le trac avant une présentation (ex. trucs de présentateur télé) : 

- Sportif de haut niveau : visualiser la veille une répétition 
- boost claping : applaudir à fond pendant 30s en criant "yes"
- Relachement des épaules (monter en inspirant, relacher à l'expiration)
- tête en bas
- Tenir 2 min la pose de Wonder Woman
- Chercher les regards bienveillants
- Nos appuis : respiration, regard et voix

### La communication

Latin _Communicare_ : mettre en commun.

#### 4 piliers pour une com saine

- L'honnêteté
- Authenticité : attention au culte du parfait, il faut jouer l'aspérité et la différence
- Intégrité
- Amour / bienveillance : vis à vis de soi et de l'autre (empêche de juger)

#### A éviter sur la parole

- Commérage : inutile, sans intérêt
- Jugement 
- Négativité → complainte
- Fausses excuses : on n'assume pas et on accuse les autres
- Broderie / exagération : on l'assimile à un mensonge
- Dogmatisme : confusion des faits et des opinions : l'autre communique son opinion en faisant croire que ce sont des faits

#### Verbal / non verbal 

Les mots sont la base du discours, importants pour raconter une histoire. Il doivent être en cohérence avec notre attitude sous peine 

Les mots sont à choisir en fonctrion d'une dominante sensorielle. 

Manque de confiance, vient souvent d'un manque de reconnaissance parentale. Et de l'absence de reconnaissance de soi

Quelle perception avez vous de vous par rapport aux autres (inférieurs / supérieurs). 
À lire : [Pilote de vie, groupe de comportement](https://anti-deprime.com/2015/09/28/ils-sont-5-et-conditionnent-toute-votre-vie-presentation-des-drivers-analyse-transactionnelle-1/) : 5 messages fréquents qui influencent nos relations :

1. Soit parfait : "tu aurais pu faire mieux"
2. Fais plaisir : "tu n'es pas gentil", peur du conflit peur de ne plus être aimé
3. Soit fort : "arrête de pleurer"
4. Fais des efforts : "tu n'as plus rien à faire"
5. Dépêche-toi : "tu n'as pas encore fini"

Ces messages peuvent devenir contraignants et deviennent néfaste s'il nous enchaînent au comportement qu'ils impliquent et génèrent un stress chronique. 

#### Paraverbal

La voix, le débit, …
Permet de faire passer émotions, sentiments, enthousiasme
L'auditoire ne doit pas faire d'effort pour nous comprendre

#### Non verbal 

Corps, visage, regard : quasi-impossible à faire mentir. Il témoigne de son état intérieur. 

Les signaux sont énormes. Exemple visage : partie droite : ce qu'on cherche à montrer, partie gauche : ce que nous vivons véritablement.

La respiration de l'auditoire se cale sur celle de l'auditeur.

### Sur le discours 

- Marqueurs logiques de progression, connecteurs (premièrement, enfin, avant tout, …) pour savoir où on en est : parler au cerveau gauche
- Adjectifs pour qualifier les choses : cerveau droit
- Importance du non verbal dans le support (les emoji, image)
- LESS IS MORE : on retient 20% d'une présentation, autant insister / répéter l'essentiel plutôt que de noyer l'information
- En présentation et conclusion : parler de l'auditoire pour le faire se sentir concerner par le speech
- Faire une vraie conclusion, pas juste un merci : 1 message, l'aboutissement du discours ou rappel des points importants
- Dans la préparation d'une présentation, l'introduction est à écrire en dernier, une fois que l'on connait tout le contenu

## Ressources

### Contenus sur la prise de parole en public

- Youtube : 
  - [Workshop - la parole en public](https://www.youtube.com/watch?v=4po9h1uYmh4)
  - [Les conseils de Fabien Olicard - mentaliste](https://www.youtube.com/watch?v=PN0YPICyIXc) : simple et concis
- Ted Talk : [Amy Cuddy: votre langage corporel forge qui vous êtes](https://www.youtube.com/watch?v=Ks-_Mh1QhMc)
- Connecteurs pour la [progression logique d'un discours](https://www.francaisfacile.com/exercices/exercice-francais-2/exercice-francais-10909.php)
- Conseil de la BPI pour [présenter oralement son projet](https://bpifrance-creation.fr/encyclopedie/porteur-projet-preparation-droits-obligations/preparation/presenter-oralement-son)

### Modèles / outils de présentation

- [Modèles de slides google / powerpoint gratuit](https://slidesgo.com/)
- Faire des présentations Web, 2 librairies dominent, avec des fonctionnalités à peu près équivalentes :
  - **RevealJS** : des slides simples et propres en HTML ou générer depuis Markdown avec Hugo, [démo revealjs](https://reveal-hugo.dzello.com/)
  - **Impress** : des slides agencés dans le plan où l'espace (3D), [démo impress](https://impress.js.org/). Il est possible de créer des présentations en ligne avec https://strut.io
