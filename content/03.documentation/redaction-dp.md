---
title: "Le dossier professionnel"
author: "Gilles Gonon"
weight: 4
chapter: false
tags: ["rapport", "doc", "pdf"]
---


##  🎓 Conseils pour le dossier professionnel 🔖

### Objectifs du Dossier professionnel

C'est un document officiel et obligatoire qui décrit vos expériences dans le domaine

Ses fonctions sont multiples : 

- Il vous prépare à l'entretien avec le jury, et facilite la prise de parole
- Il sert au jury pour mieux vous connaître
- Il servira comme CV détaillé pour une recherche d'emploi
- Il documente vos projets

### Construire son dossier

Le Dossier Professionnel est à rédiger tout au long de l'année. Les exemples intégrés doivent être conservés. 

Il présente les expériences vécues dans différentes situations :

- Travail (stage, ...)
- Bénévolat
- Formation

{{% notice info %}}
Une première présentation des DSP a été faite en décembre. Cette nouvelle version ne doit pas remettre en cause ce qui avait été fait et évalué...  
Sous réserve d’avoir effectué les corrections suggérées. 
{{% /notice %}}

Il faut juste ajouter des exemples correspondant aux nouvelles compétences acquises. 

Le dossier s’écrit au **présent**, comme le rapport de stage. 

### 👨🏾‍💼 Présenter son travail 

Présenter son travail est avant tout un **exercice de reformulation** d’un travail qui a déjà été fait. C’est aussi un travail de **présentation**, le Front-end de vos dossiers est important, il faut que ce soit illustré et agréable à lire. 

Présenter son travail à plusieurs objectifs :

- Documenter un projet réalisé
- Montrer ses compétences (acquises et/ou maîtrisées)
- Permettre au lecteur de reproduire le travail effectué (comme un tutoriel)

Pour cela, il faut d’abord donner un aperçu global du projet, expliquer en quelques phrases le contenu du projet (résumé / teaser).

Le titre des exemples doit être explicite et indiquer si dans son ensemble l’exemple est un site / une page web / des fonctions javascript ou PHP. Par exemple :

- Design Sprint : refonte du site les fourmis solidaires
- Réalisation d’un portfolio et publication sur Gitlab pages
- Codage d’une galerie d’images en PHP


### Orthographe

On ne le dira jamais assez mais l'orthographe, c'est important. 

Relisez vous, s’il vous plaît !! C'est une vraie étape de la rédaction, indépendante du contenu.

Voir les conseils de rédaction de rapports pour plus d'infos sur l'orthographe.


### Remarques diverses

Pour chaque exemple, donnez l’adresse du site réalisé dans l’exemple. Il est préférable que le site soit en ligne car il rendra compte beaucoup plus directement de votre travail.

Illustrer les exemples avec quelques captures d’écran, cela rend le document plus attractif. Il n’est pas nécessaire de mettre toutes les images en annexe, mais s’il y en a trop alors mettez les en annexe. 

Vous pouvez éventuellement rappeler l’ensemble des réalisations dans le tableau de fin *"Documents illustrants la pratique professionnelle"*.

### Ressources

- référentiel des compétences de la formation, disponible sur le NAS, ainsi que le 
- Référentiel d'évaluation
- Les informations et mode d'emploi sur le site [dossierprofessionnel.fr](http://www.dossierprofessionnel.fr/)
- Le [mode d'emploi](http://www.dossierprofessionnel.fr/pro/) pour remplir les différentes parties des exemples
- Site du Ministère du travail, [page sur les titres professionnels](https://travail-emploi.gouv.fr/formation-professionnelle/certification-competences-pro/titres-professionnels-373014)

{{%attachments title="Fichiers du référentiel" pattern=".*(pdf|odt|docx)" style="green" /%}}