---
title: "Rédiger un rapport"
author: "Gilles Gonon"
weight: 1
chapter: false
tags: ["rapport", "doc", "pdf"]
---


##  🎓 Conseils pour la rédaction d'un rapport 🔖

Un rapport doit se rédiger au **présent** ⏰, c'est plus facile à lire. 

Le rapport constitue avant tout un **dossier technique** 🩺 expliquant le travail réalisé. Ce n'est pas un carnet de bord où l'on explique ce que l'on a fait chaque jour. 

C'est une **synthèse** du travail réalisé. Il faut donc d'abord expliquer de manière détaillée le projet à réaliser puis la manière dont vous avez réalisé chaque partie.

Globalement, il vaut mieux faire des **phrases assez courtes** 🔪 qui seront plus compréhensibles. Dans un rapport technique, on utilise aussi beaucoup les 👉 **listes à puces** pour énumérer les idées, réalisations, différents points. 

Mettre toutes les idées dans une phrase devient vite incompréhensible et pas facilement mémorisable 🤯. 

{{% notice tip %}}
Il faut faciliter la vie au lecteur 🦥, par le format du document et un contenu synthétique.
{{% /notice %}}

### Quelques infos sur les différentes parties

#### 🕌 La page de garde 

Elle donne la première impression sur le rapport, faire simple, beau et complet. 

🔥 L’idéal est **1 seule** page de garde

#### Le sommaire

C'est une liste numéroté des parties, section du rapport avec un lien vers les numéros de page afin d'aller rapidement au contenu souhaité. 

Il doit être généré automatiquement ⚡, sous peine d'être incorrect (dans 90% des cas). 

La **numérotation des parties** 🎰 : pour les rapports il est standard d'utiliser une numérotation avec des numéros uniquement : 1., 1.1, 1.1.1, 1.1.1.1, … Cela permet de savoir facilement où l'on se situe dans le rapport. 

Il est standard aussi que le sommaire contienne des liens 🔗 vers les pages du rapport. 

Dans open office, cette page donne des explications pour [ajouter les liens au sommaire](https://wiki.openoffice.org/wiki/Documentation/FAQ/Writer/IndexToc/How_do_I_make_my_Table_of_Contents_be_hyperlinks_to_the_relevant_part_of_the_document).

#### Le résumé

Il est très important car c'est votre pitch de base pour expliquer le travail que vous avez fait durant votre stage 👥. 

⏳ ça doit prendre ~1 minute à lire. Il présente en général :

- le sujet du travail en 1 phrase, 
- puis le contexte (où et comment a été réalisé le stage, seul ou à plusieurs, suivi par qui)
- les réalisations majeures (principaux résultats, site, documentation, ...) 
- les langages et principaux outils pour réaliser le stage. 

Ce n'est pas un sommaire ni une introduction.

#### L'introduction 

Elle présente : 

- le contexte du stage 🤝, 
- 🔦 Les thèmes abordés dans chaque partie et comment elles s'articulent 
- Il ne faut pas dévoiler le contenu des parties 🙈.  

#### Les annexes 

On met en annexe des documents moins importants pour la compréhension du rapport. Pour chaque annexe, il faut donner un titre et au moins une ligne de description de ce qu'elle contient. 

> Ce sont des choses *annexes*

Ce n'est pas une partie où l'on met tout ce qui ne tient pas dans le rapport. 

Typiquement elles contiennent : 

- plus d'explications pour un lecteur intéressé qui veut en savoir plus sur un sujet par exemple. 
- des documents joints fournis par les clients, s'ils ne sont pas trop volumineux. 
 
#### Ressources / bibliographie 

Elles identifient tous les documents que vous avez utilisés pour la réalisation de votre travail.

Pour la réalisation de sites web, il s'agit souvent de ressources en ligne. Dans ce cas, indiquer les titres des pages et liens vers les sites, ainsi que l' auteur / éditeur. 

{{% notice tip %}}
C'est aussi l'occasion de montrer que vous avez consulté des sites en _anglais_. 
{{% /notice %}}

Par exemple :

- Documentation Bootstrap version 4.4 : https://getbootstrap.com/docs/4.4/, document en anglais
- Catalogue d'icônes FontAwesome : https://fontawesome.com/icons?d=gallery
- Documentation PHP version X.X en ligne : https://www.php.net/manual/fr/
  
#### Livrables du stage

Il faut rajouter au rapport une section, avant la conclusion, indiquant les livrables fournis au client : 

- Lien vers le site réalisé si disponible, 
- Fourniture d'un prototype ...
- Documentation utilisateur
- Documentation et code sources du projet :  qu'est-ce qui est envisagé pour la maintenance du projet


#### Jeu d'essai d'une fonctionnalité

- 🛡️ But n°1 : parcourir tous les cas d’un code
- 1 jeu de test = plusieurs cas 🎛️
- Quand ça marche 😀
- Quand ça ne marche pas, mais que c'est prévu 🤕
- Exemple : saisie OK et KO dans un formulaire

## Règles typographiques

### Ponctuation

- Pas de ponctuation à la fin d'un titre : pas de ':' ou de '.' à la fin d'un titre, éventuellement '?' ou '!'
- On met entre parenthèses des choses peu importantes, voir on ne les mets pas du tout

### Inclusion de code

Pour **les inclusions de lignes de code ou commandes informatiques** : 

- Isolez les lignes de code, dans un paragraphe séparé du texte
- 🎨 Utilisez la coloration syntaxique

Il est dommage de mettre une capture d'écran du code réalisé dans un éditeur. Utilisez plutôt la coloration syntaxique ! Cela permet au lecteur de tester vos bouts de codes par simple *copier-coller*. 

{{% notice tip %}}
Pour insérer du code formaté dans un rapport, il suffit de **faire un copier-coller depuis Visual Studio** (utilisez pultôt un thème clair).
{{% /notice %}}


## Remarques diverses

Le fait d'utiliser Gitlab n'indique pas que vous faites de l'intégration continue. C'est par défaut uniquement du suivi de version. Pour rappel : 

- **Intégration continue (CI)** : L'intégration continue consiste à faire de manière automatique des tests de non régression sur le code suivi à chaque commit sur le serveur. 
- **Déploiement continu (CD)**, c'est quand le code est mis en production s'il passe les tests. Pour un site, il est mis en ligne (exemple des git pages). 


## Orthographe 

{{% notice warning %}}
🙏 Relisez vous, s'il vous plait !!  
**C'est une vraie étape de la rédaction**, indépendante du contenu.  
En milieu **professionnel**, on vous passera quelques erreurs (disons 1 ou 2 par page), mais pas de grosses fautes 🤬.
{{% /notice %}}

💡 Si vous avez des faiblesses en orthographe 🤒, indiquez-le aux gens avec qui vous travaillez pour qu'ils vous relisent systématiquement 🧐. 

{{% notice tip %}}
Il vaut mieux prévenir que d'être mal jugé. 
{{% /notice %}}

Vérifiez un par un :

- ✅ Les pluriels des noms et des adjectifs
- ✅ Les accords des verbes 
- ✅ Les participes passés et infinitifs
- ✅ Posez vous la question du verbe être ou avoir aussi souvent que nécessaire 


## Les explications en vidéo

{{< youtube IYBoLbGriSk >}}
