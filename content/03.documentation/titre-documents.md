---
title: "Nommage des documents électroniques"
author: "Gilles Gonon"
weight: 3
chapter: false
tags: ["rapport", "doc", "pdf"]
---


##  🎓 Conseils pour nommer vos rapports / documents 🔖

Il faut donner un titre explicite à vos documents. Mettez vous à la place du lecteur qui se retrouve à lire 15 fichiers nommés "rapport.pdf", comment s’y retrouve-t il ?

Typiquement les noms de vos fichiers doivent être composés de différents champs :

- Le type du document : `Rapport de stage`, `Dosier Professionnel`, `Lettre`, ...
- Objet du document par exemple `Site Entreprise SoupAstek`, `Wordpress Asso FootClub`, ...
- Votre nom, `Robert Durant`, ...
- La date de modification (yyyy-mm-jj) : elle permet de trier les versions plus récentes facilement.
- Éventuellement d'autres infos : `Relecture GG`

Pour être plus lisible, on sépare les champs par un symbole, `-`, `.`, `_`

Par exemple :

- Rapport de Stage site Socotec - Alfred Jary - 2020-04-14.pdf
- Dossier Professionnel - DevWeb - Bernard Minet - 2020-04-09.pdf
- Lettre démission - Beaumanoir - Henri Bernard - 2020-05-19.odt
- Comptabilité - Association Foot - Zinedine Messi - 2018-02-16.xlsx

Les titres à éviter sont :

- Rapport (4).pdf
- Mon Rapport de stage.pdf
- Dossier professionnel (trame).pdf

{{% notice warning %}}
Quand vous laissez “trame” ou “modèle” dans un nom de fichier, c’est un peu comme quand vous laissez l’étiquette d’un habit que vous venez d’acheter.
{{% /notice %}}

