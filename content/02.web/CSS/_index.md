---
title: "CSS"
weight: 2
author: [Gilles Gonon]
tags: [css, responsive, mediaqueries, adaptatif, design]
chapter: true
---

Les feuilles de styles appliquées aux balises HTML permettent de modifier intégralement l'aspect d'une page.

{{% notice tip%}}
Soyez vigilants à toujours bien séparer  :

- le contenu = les pages HTML
- et la forme = les feuilles de styles = fichiers CSS
{{% /notice %}}
