---
weight: 20
title: "Design adaptatif"
author: [Gilles Gonon]
tags: [css, responsive, mediaqueries, adaptatif, design]
summary: "Cours et exemples de design responsive pour les différentes tailles d'écran en utilisant les médias queries"
---

## Inspiration

Le design responsive consiste à s'assurer qu'une page web se redimensionne bien en fonction de la taille de l'écran.

![Tailles typiques en pixels des différents écrans - Wikimédia](/cours-giloop/images/responsive-screen-sizes.png)

Un design adaptatif va un peu plus loin en proposant de réorganiser et sélectionner le contenu en fonction de la taille d'écran et du média de visualisation.

Pour comprendre la différence entre les deux :

- consultez [https://zurb.com/responsive : quelques exemples de sites au design adaptatif](https://zurb.com/responsive).
- [https://mediaqueri.es/](https://mediaqueri.es/)

## Introduction

Les feuilles de styles CSS permet de mettre en forme une page. Pour cela, le code HTML est organisé en **blocs**. Ces blocs vont être très importants et reliés au **wireframe** de votre page.

Deux outils CSS vont être indispensables pour utiliser et maîtriser le design :  Flexbox et les grilles CSS.

L'outil pour gérer des pages adaptatives est les **media queries** qui permet d'affecter un style particulier en fonction de la taille de l'écran.

## Exercices

### Veille

- Bootstrap est un framework CSS pour faire des pages responsives, pouvez-vous en trouvez d'autres équivalents voire meilleurs ?
- Une présentation de Morten Rand-Hendriksen à Wordcamp Europe 2017 explique bien les enjeux autour des grilles CSS :
  - Version courte : https://www.youtube.com/watch?v=ojKbYz0iKQE
  - Version longue : https://www.youtube.com/watch?v=txZq7Laz7_4
  - Ressources associées sur son site : http://mor10.com/wceu2017/

### Connaître la taille des écrans

- Recherchez la taille des écrans standards. C'est un peu un casse-tête entre la taille de l'écran en pixel et la résolution de l'écran. On gardera comme référence la taille disponible.
- Cet [article du site CSS tricks](https://css-tricks.com/snippets/css/media-queries-for-standard-devices/) donne par exemple l'ensemble des tailles standard d'écrans
- En Javascript, il est possible de connaître la taille disponible de l'écran. Testez la fonction suivante :

```javascript
<p>Deux fonctions Javascripts permettent d'obtenir la taille d'un écran</p>
<p>La taille disponible (AVAILABLE)</p>
<p id="availableSize"></p>
<p>La taille absolue </p>
<p id="absoluteSize"></p>

<script>
    function afficherTaille() {
        document.getElementById('availableSize').innerHTML = `Height:${window.screen.availHeight}, width:${window.screen.availWidth}`
        document.getElementById('absoluteSize').innerHTML = `Height:${window.screen.aheight}, width:${window.screen.width}`
    };
    afficherTaille();
</script>
```

Pour connaître la taille de la fenêtre du navigateur vous pouvez utiliser les propriétés suivantes `document.body.clientWidth` et `document.body.clientHeight` 

Enfin, si vous utilisez jQuery, vous pourrez alors simplement ajouter un évènement pour surveiller la taille de la fenêtre et adapter le site dynamiquement (dans les rares cas où le CSS ne suffit pas) :

```js
// Utilise jQuery
var _innerWidth = $( window ).width();

// Fonction callback appelée en cas de redimensionnement de la fenêtre
$( window ).resize(function() {
    var newWidth = $( window ).width();
    if (_innerWidth >= 820 && newWidth < 820) {
        // A faire lorsque la taille devient petite (<820px)
        // ...
    } 
    // Mise à jour de la largeur
    _innerWidth = newWidth;

  });
```

### Designer avant de coder

Reprenez vos portfolios et créer 2 wireframes d'organisation des blocs de votre page :

- un pour la version mobile du site : le css doit être **mobile first**
- un pour la version PC/tablette : adapter la mise en page pour les PC , une feuille de style css par média ciblé :

```css
@media screen and (min-width:800px) { /* Celui là est suffisant pour les écrans larges */ }
@media screen and (min-width:1200px) { /*  */ }
```

[Le site detectmobilebrowser](http://detectmobilebrowsers.com/) fournit des scripts dans différents langages pour détecter sur quel type de mobile vous êtes.

## Optez pour les grilles CSS

Les grilles CSS et Flexbox sont de plus en plus utilisés pour la mise en page des pages web. Si le framework Boostrap inclut de nombreuses fonctionnalités et permet de faire des mises en place très modulaires mais au prix d'une complexité au niveau des balises div.

Flexbox permet de positionner les éléments.
Les grilles permettent de gérer la mise en page globale en 2D.

N'oubliez pas les frameworks CSS qui vont vous simplifier la tâche.

## Ressources

- [Présentation des grilles CSS](https://www.mozilla.org/en-US/developer/css-grid/) par Mozilla. En bas de page, il y a  d'autres liens intéressants.
- [Passer de Bootstrap aux grilles CSS](https://hacks.mozilla.org/2017/04/replace-bootstrap-layouts-with-css-grid/) : article en anglais sur Mozilla Hacks
- Des thèmes Bootstrap
- **[bootstrap](https://getbootstrap.com)** : framework css pour site responsive et fonctionnalités de mises en forme javascript d’éléments classiques de pages web (tables, images, carrousel,  barres de progression , alertes, ...). Regarder notamment les pages d'[exemples bootstrap](https://getbootstrap.com/docs/4.3/examples/).
- [Zurb foundation](https://foundation.zurb.com/)
- [Bulma](https://bulma.io/) : open source CSS framework qui monte
- [Skeleton](http://getskeleton.com/) : un petit framework responsive
- [Materialize](https://materializecss.com/) est le framework de Google qui implémente les recommandations en termes de design UI.
- [Normalize](https://necolas.github.io/normalize.css/) : feuille CSS pour normaliser le rendu des éléments HTML sur les différents navigateurs.  

## Sass : un préprocesseur CSS

### Présentation

[**Sass** (Syntactically Awesome Stylesheets)](https://sass-lang.com/) est un langage dynamique de génération de feuilles de style en cascade. C'est un préprocesseur CSS, un peu comme PHP est un préprocesseur de HTML. Dans le cas de Sass, la compilation n'est pas dynamique sur le serveur. En production, les feuilles de style sont compilées une fois pour le site, et recompilées puis les fichiers CSS générés sont uploadés sur le serveur.

Les feuilles de styles sont écrites dans un langage de description compilé en CSS. Pour faire simple, la syntaxe est proche du CSS mais avec la possibilité d'ajouter des variables et d'inclure plusieurs feuilles de styles à la compilation dans un gros fichier CSS. Cela permet de modulariser les fonctionnalités et de les maintenir plus facilement dans des fichiers séparées.

**Remarque** : un autre préprocesseur répandu est **Less**. Il semble [cependant moins utilisé que Sass](https://css-tricks.com/sass-vs-less/).

### Installation

Voir le site de Sass pour comment l'[installer](https://sass-lang.com/install) sur les différents systèmes

```bash
# Windows (Sass+Dart)
choco install sass
# NodeJS
npm install -g sass
# MacOS
brew install sass/sass/sass
```

Pour tester si l'intallation a bien fonctionné. Créer le fichier `input.scss` :

```scss
$font-stack:    Helvetica, sans-serif;
$primary-color: #333;

body {
  font: 100% $font-stack;
  color: $primary-color;
}
```

Dans un terminal, lancer la commande suivante :

```bash
sass input.scss output.css
```

Ce qui doit générer la sortie suivante :

```css
body {
  font: 100% Helvetica, sans-serif;
  color: #333;
}
```

Il est également possible d'utiliser un mode _live_ (recompilation à chaque sauvegarde / modification des fichiers scss) avec l'optiob `--watch`. Cela permet d'inclure le CSS compilé dans vos pages et de travailler sur le code source en Scss.

```bash
sass --watch scss/style.scss css/output.css
```

### Utilisation

Parcourir la [documentation de Sass](https://sass-lang.com/guide) pour tester les principales fonctionnalités :

- Utilisation de _variables_, notamment pour les couleurs du thème et les fontes
- Possibilité d'imbriquer des blocs (_nesting_) pour une meilleure lisibilité
- Séparation du code en _modules_ (fichiers) importé avec la commande ``@use 'base';`` (par ex pour importer un fichier base.css)
- Utilisation des ``mixins`` ou regroupement de propriétés css que l'on inclut dans d'autres blocs
- Héritage et extension : Regroupement de propriétés en classes qui peuvent être appelées ensuite dans des blocs.
- Utilisation des opérateurs mathématiques
- Possibilité de créer des fonctions

### Pour aller plus loin

Les différents Framework sont pour la plupart fournis en version distribuable (dist) ou en code source sous forme Sass. Il est possible d'inclure les sources dans votre projet et de modifier en live les propriétés, par exemple les couleurs dans le code SCSS.

Un [guide de style de codage Sass](https://sass-guidelin.es/fr/) est réalisé par [Hugo Giraudel](http://hugogiraudel.com/), traduit en francais. C'est une bonne lecture pour appréhender le SCSS et coder proprement.  

## Frameworks CSS

### Bootstrap

Un des avantages de Bootstrap est la fourniture de nombreux exemples plus ou moins complets : thèmes gratuits, de snippets (startBootstrap, bootswatch).

### Materialize

Un des avantages de Materialize est la charte graphique épurée, le design propre et des outils de mise en page standards pour les applications web.

- Installation / documentation : https://materializecss.com/
- https://pixinvent.com/materialize-material-design-admin-template/documentation/getting-started.html

### Material Design

https://mdbootstrap.com/?utm_ref_id=26938
