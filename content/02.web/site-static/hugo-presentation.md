---
author: ["Gilles Gonon"]
title: "Faire des présentation Web"
weight: 4
chapter: false
tags: ["web", "staticsite", "hugo", "revealjs"]
---

## Faites vos présentations Web avec RevealJS et Hugo

### Petit tour d'horizon 🧭

[RevealJS](https://revealjs.com/) est une librarie javascript pour créer des présentations type Powerpoint depuis du HTML. Vious pouvez ainsi diffuser vos supports depuis un navigateur et utiliser les mêmes ressources qu'une page Web, diffuser facilement des contenus multimédia, sons, images, vdéos et bien sur des GIFS.

Le meilleur moyen de voir l'ensemble des fonctionnalités est bien de regarder la présentation sur la page d'accueil du site [RevealJS](https://revealjs.com/).

Pour aller plus vite dans l'édition de vos présentations, je vous propose de travailler directement dans des fichiers Markdown et de générer le site à l'aide de Hugo

### Utilisation de Hugo

#### Installation 😱

[Hugo](https://gohugo.io/) est un [générateur de sites statiques](https://www.staticgen.com/), c'est à dire un logiciel qui crée un site entier à partir d'un template afin que vous ne nous focalisiez que sur le contenu.

Le meilleur moyen d'installer Hugo est d'utiliser un gestionnaire de paquets. Il sont natifs sous Linux, et doivent être installés sous MacOS (par exemple [HomeBrew](https://brew.sh/)) et Windows ([Chocolatey](https://chocolatey.org/) ou [Scoop](https://scoop.sh/)).

Suiver les instructions sur le site de Hugo et poursuivez.

#### Créer votre site

Pour créer votre site, lancer un terminal de commande dans le dossier où vous souhaitez créer votre projet de présentation web qui sera dans un sous-dossier.

🔥 Astuce sous Windows : ouvrez l'explorateur, faire un clic droit dans le dossier qui contiendra votre projet et cliquez sur `Git Bash Here`.

{{% notice tip %}}
Bonnes pratiques : donner un nom de projet en minuscule, sans accent ni espace
{{% /notice %}}

```bash
hugo new site ma-presentation
```

Un dossier "ma-presentation" est créé qui contient le début de votre site.

#### Télécharger le thème reveal

Il faut maintenant ajouter le thème [Reveal hugo](https://themes.gohugo.io/reveal-hugo/) dont vous pouvez voir une démo [ici](https://themes.gohugo.io/theme/reveal-hugo/).

Nous allons bien sur ajouter un suivi de version

```bash
cd ma-presentation
git init
git clone https://github.com/dzello/reveal-hugo.git themes/reveal
```

Ceci créé un sous-dossier `themes/reveal` dans votre dossier `ma-presentation` qui contient l'ensemble des données de mises en page.

🧙 Pour les plus courageux d'entre vous, qui veulent s'améliorer en git, je vous encourage à utiliser le thème sous forme de sous-module git. Remplacez la derniére ligne, le `git clone ...` par :

```bash
git submodule add https://github.com/dzello/reveal-hugo.git themes/reveal
```

#### Configuration du thème 🔧

Quand vous téléchargez un thème Hugo, ils contiennent un sous-dossier `exampleSite` qui permet de voir comment fonctionne le thème en fournissant un exemple de configuration et du contenu.

Il faut recopier les dossiers qu'il contient (à l'exception de .forestry) dans le dossier racine de votre projet Hugo, ici `ma-presentation` en écrasant le contenu existant.

![dossiers-example-site-reveal-hugo.png](/cours-giloop/images/dossiers-example-site-reveal-hugo.png)

🧙 Pour la suite nous allons travailler dans Visual Studio Code !

Editez le fichier `config.toml` à la racine, et rentrez les modifications suivantes :

```toml
baseURL = "/"
languageCode = "fr-FR"
title = "Ma super présentation Reveal"
disableKinds = ["sitemap", "RSS"]
theme = "reveal"
themesDir = "themes"
```

#### 🤓 Voir le site

Dans le terminal de Visual Studio Code, tapez :

```bash
hugo server -D
```

Cela génère le site et lance un Live Server pour voir le résultat en local. L'adresse est indiquée à la fin de la génération :

```bash
M:\IMTS\ma-presentation>hugo server -D
Building sites …
                   | EN
-------------------+------
  Pages            |  11
  Paginator pages  |   0
  Non-page files   |   4
  Static files     | 162
  Processed images |   0
  Aliases          |   0
  Sitemaps         |   0
  Cleaned          |   0

Built in 255 ms
Watching for changes in M:\IMTS\ma-presentation\{archetypes,assets,content,data,layouts,static,themes}
Watching for config changes in M:\IMTS\ma-presentation\config.toml
Environment: "development"
Serving pages from memory
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at //localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

Vous pouvez maintenant voir le site en Live Server à l'adresse : [http://localhost:1313](http://localhost:1313) . Les modifications que vous réalisez par la suite sont directement prises en compte.

Vous retrouvez alors la démo du thème. Parcourez-là, elle contient l'ensemble de la documentation nécessaire pour créer vos présentations.

### C'est à vous 😵

#### Premiers pas 👣

##### `_index.md`

Vous n'avez plus alors qu'à modifier les fichiers du dossier `content` où sont rassemblés les contenus de votre site.

Dans Hugo, le fichier `_index.md` doit être présent dans chaque sous dossier. il correspondra à votre fichier `index.html` dans le site généré.

🔥 Les fichiers md sont constitués d'une entête au format [YAML ou TOML](https://gohugohq.com/howto/toml-json-yaml-comparison/) dont le contenu est très important, il définit toutes les options du thème et de RevealJS.

Si vous modifiez ce fichier, les modifications prennent directement effet dans le live server.

Modifiez le contenu du fichier `_index.md_` après l'entête comme suit :

```markdown
# 📽️

# Ma présentation ✊

C'est parti mon **Kiki**.

~ made by [@giloop](https://gueulomaton.org/) ~

---

# Ça continue

## Niveau 2

### Niveau 3

Ceci est un nouveau transparent, séparé par 3 tirets `---` entourés de sauts de lignes
```

#### Et ensuite ?

Pas de miracle, il va falloir lire la documentation ensuite pour voir toutes les fonctionnalités.
Un dépôt github recensant des [bonnes pratiques pour Hugo](https://github.com/spech66/hugo-best-practices).

> Et je vous assure qu'elles sont nombreuses  
> _Giloop_
