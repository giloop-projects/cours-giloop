---
title: Les sites statiques
weight: 10
chapter: false
---


## Présentation

Un site statique contient uniquement des pages Web codées en HTML utilisant CSS et Javascript. Ils s'opposent aux sites dynamiques où les pages sont générées côtés serveur avant d'être envoyées au navigateur. Au début du Web, il n'y avait que des sites statiques, puis les sites dynamiques sont arrivés ajoutant de nombreuses fonctionnalités et une expérience utilisateur personnalisée.

## Avantages et inconvénients

Il y a cependant un retour d'intérêt grandissant pour ce type de site **en apparence** limité en fonctionnalités. Les sites statiques présentent des avantages importants :

- Rapidité : l'absence de base de données et de préprocesseur, rend le site plus rapide. Il est aussi possible de jouer sur la [mise en cache](https://developer.mozilla.org/fr/docs/Web/HTTP/Cache) des éléments du site pour arriver à des sites très rapides. Dans les tests de performances le _Time to First Byte_ **TTFB** est au plus bas.
- Simplicité d'hébergement : il faut simplement copier les fichiers ce uiq peut être facilement intégré dans une démarche d'intégration continue. Le site peut même être mis sur un CDN pour un service ultra rapide et localisé.
- Sécurité : vous êtes toujours à jour ! Avec un site dynamique, les failles de sécurité apparaissent et nécessitent des mises jours qui sont rarement effectuées. Pour un site statique, ce problème n'existe pas, et on parle de plus en plus de solution sans serveur, _serverless_. Voir [Netlify](https://netlify.com)
- Une nouvelle expérience développeur. L'expérience utilisateur est une chose, l'expérience développeur, la votre en est une autre. Une fois la chaine d'intégration mise en place, vous êtes concentrés sur le développement de la mise en forme du site et son contenu. Les [générateurs de sites statiques](https://www.staticgen.com/) seront vos amis pour automatiser les tâches ingrates de génération et déploiement du site.
- Une aide abondante est disponible en ligne dans la communauté des développeurs de générateurs de sites statiques. Le terme [JAMStack](https://jamstack.org/), proposé initialement par Netlify, sera votre point d'entrée pour comprendre l'ensemble des possibilités des **sites statiques**.

Il y a bien sûr aussi quelques inconvénients :

- Vous devez regénérer le site à chaque modification du contenu
- Le contenu ne sera pas personnalisé pour les différents utilisateurs
- Les fonctionnalités backend et la gestion de formulaires et connexion sont plus compliquées et souvent délégués à des API tierces, qui induisent une dépendance sur votre site.

## Synthèse

En conclusion, il est important de connaître ces nouvelles méthodes de générations de sites statiques et il est probable que les meilleurs sites seront des mixs entre sites statiques et dynamiques, avec l'utilisation renforcée de microservices pour gérer les fonctionnalités backend de votre site.

## Ressources

Ces notes sont inspirés d'un très bon [article sur Scotch.io](https://scotch.io/bar-talk/5-reasons-static-sites-rock).

Le site [JAMStack.org](https://jamstack.org/) rassemble de nombreuses infos et plein d'[exemples](https://jamstack.org/examples/) sur cette nouvelle manière de faire un site et de l'**héberger**.

Un livre complet et gratuit : [Modern Web development on the JAMStack - O'Reilly](https://www.netlify.com/pdf/oreilly-modern-web-development-on-the-jamstack.pdf)

Des ressources en termes d'API et services Web

- [10 APIs pour vos sites statiques en 2020](https://www.stackbit.com/blog/extending-jamestack-2020/) : article de blog en anglais
- [The new Dynamic, faster websites](https://www.tnd.dev/)
- [Awesome Static Website Services](https://github.com/agarrharr/awesome-static-website-services) : page Github de liens "fabuleux"
- [Awesome](https://awesome.re/) : liste de ressources pour le dev, énorme
- [Site de Josh Habdas](https://habd.as/) : des ressources pour codeurs (avancés). Il a par exemple créé le thème After Dark pour Hugo, dont les performances Lighthouse sont très très bonnes !
