---
title: "Les bases de données relationnelles"
author: ["Gilles Gonon"]
tags: ["database", "bdd", "db", "données", "sql"]
description: "Cours sur les bases de données relationnelles"
title: "Bases de données"
weight: 10
chapter: false
draft: true
---

## Cours BD relationnelles

![(Base de données)](/cours-giloop/images/database-icon.png)

## Introduction

### Qu'est-ce qu'une donnée ?

![Le grand jeu de la data](/cours-giloop/images/data-dice.png)

> Une donnée est un objet, une chose qui contient de l'**information**.

#### Quelques exemples

- un texte
- un tableau
- des valeurs physiques lues par des capteurs
- une image
- un son
- ...

On s'intéressera aux données **numériques**, ce qui impose avant une phase de numérisation ou digitalisation de la donnée, qui peut lui faire perdre de l'information (précision).

Les données numériques sur lesquelles on va travailler se présentent sous différentes formes

- des tableaux de valeurs (csv, excel, ...)
- des séries temporelles (temps ou date + valeur) : par exemple un son
- des textes libres non structurés
- des images, non structurées

#### Les données ouvertes

>Une donnée ouverte est une donnée qui peut être librement utilisée, réutilisée et redistribuée par quiconque - sujette seulement, au plus, à une exigence d’attribution et de partage à l’identique.

Différentes familles de licences :

- Privées : pas le droit d'utiliser les données (copyright)
- [Creative commons](http://creativecommons.fr)
- Open source : GPL, CECIL, MIT, Qt
- Copyleft : BSD, art-libre  

TODO : expliquer la RGPD (réglementation générale de protection des données).

### Les bases de données

Les bases de données rassemblent des données avec des informations sur leur format et leur organisation. Elles permettent d'organiser des données pour y accéder rapidement et précisément via des **requêtes**.

Bases de données relationnelles, temporelles, temps-réel, ...

## Notions sur les BD relationnelles

Info en partie issue des cours

- [Openclassroom](https://openclassrooms.com/fr/courses/4449026-initiez-vous-a-lalgebre-relationnelle-avec-le-langage-sql/)
- [Developpez.com](https://sgbd.developpez.com/cours/)

### Vocabulaire des BD relationnelles

- SGBD : Système de gestion de base de données
- Une _relation_ est souvent appelée *table*
- Un _tuple_, _n-uplets_, _enregistrement_ ou _vecteur_ est une *ligne d'une table*, noté (pomme, verte, 300)
- une _colonne_ est un _attribut_. Les colonnes sont consistantes, chacune de ses lignes contient le type d'information (le nom, l'age, le prix HT, ...)
- un _schéma_ est l'ensemble des attributs d'une table
- un _domaine_ est l'ensemble des valeurs que peut prendre un attribut, le domaine peut également inclure des _contraintes_ pour assurer la validité des valeurs
- la **clef** permet d'identifier un tuple pour y accéder : c'est un groupe d'attribut minimum déterminant un tuple unique

En base de données, on choisit une clef parmi les candidats, c'est la **clef primaire** !  
Une **clef primaire** peut être composée de plusieurs attributs, mais en pratique on utilise un attribut artificiel (ID entier) qui permet d'identifier simplement les enregistrements.

Une **clé étrangère** est une colonne (ou l'ensemble des attributs définissant la clef) référençant une clef primaire d'une autre table.

### Relation et table d'association

#### Définition

Aussi appelée table de composition. Cette table sert à relier les éléments de plusieurs tables.  
Dans cette table, la clef primaire est composée d'au moins deux clefs étrangères qui constituent la relation. Si on ajoute d'autres champs, (ex: date de la relation, commentaires, ...) ils peuvent être nécessaires pour définir la clef primaire (la date par ex).  

#### Types de relation

On parle de **cardinalité** pour les différents types de relation entre des différentes tables.

Les différentes cardinalités sont :

- **de 1 à plusieurs** (ex : un client passe plusieurs commandes)
- **de plusieurs à 1** (ex: plusieurs produits contituent une commande)
- **de plusieurs à plusieurs** (ex : plusieurs pommes pour 1 personne, plusieurs personnes sur 1 pomme)
- **de 1 à 1** (ex : une pomme par personne, et basta !)

![Relation-1-n.png](/cours-giloop/images/Relation-1-n.png)  
![Relation-1-1.png](/cours-giloop/images/Relation-1-1.png)


#### Modèle Entité Association (ERD)

Le [Modèle Entité Association](https://fr.wikipedia.org/wiki/Mod%C3%A8le_entit%C3%A9-association)

Les notations varient d'un logiciel à l'autre. Ce site présente différentes notations https://www.er-diagram.com/database-diagram-notation/

![notation-entity-relation-diagram.png](/cours-giloop/images/notation-entity-relation-diagram.png)

Cette [vidéo](https://www.youtube.com/watch?v=QpdhBUYk7Kk) d'explication est très claire, mais en anglais.

Pour trouver la cardinalité entre 2 tables, poser la question : quel dans la relation les nombres _min_ et _max_ de chaque entité qui peuvent intervenir.

![exemple-ERD.png](/cours-giloop/images/exemple-ERD.png)

![diagramme-erd.png](/cours-giloop/images/diagramme-erd.png)

### Table d'association

Aussi appelée table de composition. La question de la cardinalité est celle de la relation entre les individus des différentes tables.
Les différentes cardinalités sont :

- **de 1 à plusieurs** (ex : une espèce pour plusieurs individus)
- **de plusieurs à 1** (partage d'un individu en plusieurs)
- **de plusieurs à plusieurs** (ex : plusieurs pommes pour 1 personne, plusieurs personnes sur 1 pomme)
- **de 1 à 1** (ex : une pomme par personne, et basta !)

Dans cette table, la clef primaire est composée au moins des deux clefs étrangères qui constituent la relation. Si on ajoute d'autres champs, (ex: date de la relation, commentaires, ...) ils peuvent être nécessaires pour définir la clef primaire (la date par ex). 

### Redondance et normalisation

Il serait possible de créer une table contenant toutes les infos de chaque enregistrement. Mais certaines infos peuvent être factorisées. On va donc créer une table dès que des infos sont dupliquées.

Pour identifier les infos à séparer dans des tables on peut se poser les questions suivantes :

> '' l'attribut est-il relatif à un enregistrement/individu spécifique ou est-il d'ordre générale (propriétés de classes, ou relatifs au type d'objet) ? ''
> "Si je modifie une valeur d'attribut d'un enregistrement, est-ce que je dois changer les valeurs à d'autres lignes"
> "Si j'ajoute / supprime une colonne, est-ce qu'elle dépend de manière unique de la clef primaire ? "

Dans le domaine des bases de données, le fait d'enlever les redondances s'appelle la **normalisation**.

Il existe différents niveaux de formes normales qui correspondent à l'optimisation du modèle relationnel. Les 2 premières sont importantes, la 3è est une violation de la normalisation qui permet de résoudre des cas particuliers.

| id       | pseudo      | nom           | email         |
|:--------:|:-----------:|:-------------:|:-------------:|
| 1        | ed          | Ed Doe        | e.d@d.org     |
| 2        | gg          | Gil Go        | gg@gg.org     |

1. **1FN** : chaque attribut doit être atomique :

    - ex 1 : l'attribut `nom = M. Gilles Gonon` est dit multivalué car si on le décompose on a le nom, le prénom et la civilité
    - ex 2 : si une personne à plusieurs mails, on fera une table d'association plutôt que de recopier des  lignes ou d'ajouter des colonnes avec le nombre max d'emails (email_1, email_2, ...)  
2. **2FN** : pour les tables avec clefs primaires composites : les attributs ne faisant pas partie de la clé primaire doivent dépendre de toutes les parties de la clé primaire. Sinon, il faut créer une nouvelle table pour l'attribut et la clef considérée (table d'association).
3. **3FN** : Comme la 2FN mais entre attributs non clefs. Si un attribut peut Si un attribut peut être déduit des autres on l'enlève. Parfois, on préfère quand même le garder pour ne pas avoir à le recalculer à chaque requête. Ex : montant TTC d'une facture calculé à partir du montant HT qui peut être utile pour les recherches.

| id\[PK] | civilité | nom      | sexe |
|---------|----------|----------|------|
| 1       | monsieur | Latouche | M    |
| 2       | madame   | Latouche | F    |
| 3       | monsieur | Maire    | M    |

### Les opérateurs

Chapitre pour ceux qui veulent aller plus loin.

- **union** : concaténation de 2 tables de même schéma
- **différence** : (R2-R1) renvoie les tuples de R2 qui ne sont pas dans R1, non commutative : (R2-R1) != (R1-R2)
- **produit cartésien** : le produit cartésien entre deux relations R1 et R2 de schémas différents est composé de toutes les combinaisons possibles entre les tuples de R1 et les tuples de R2
- La **division** d'une relation R1 par une relation R2 (sachant que R1 et R2 ont au moins un attribut commun) donne une troisième relation R3 constitué des attributs de R1 qui n'appartiennent pas à R2, et qui contient un sous ensemble des tuples de R1 obtenus par assemblage avec ceux de R2. La division peut également générer un reste R4 de même schéma que R1 tel que : R1 = R3 * R2 +R4  

Les opérations qui en découlent sont :

- **jointure** : Elle permet de générer la table/relation entre deux tables liées par une table d'association. enchaînement d'un produit cartésien et d'une restriction. Elles peuvent être :  
  - **jointure interne** :
  - **jointure externe** : opération orientée :
  - gauche :
  - droite :

## Quelques exemples

- Diagramme entité association de la [BDD de Wordpress](https://codex.wordpress.org/Database_Description), ou [ici](https://www.erdcloud.com/d/Jwocm5MbRmWoitncA).
- Recherche [DuckDuckGo sur les exemples d'ERD](https://duckduckgo.com/?t=ffab&q=erd+examples&iax=images&ia=images)
- [Quelques explications](https://www.edrawsoft.com/fr/entity-relationship-diagrams.php)

## Créer une base de données

### Modéliser  

La conception d'une base de données passe par une **phase de réflexion**. Il est nécessaire de modéliser la base en concevant le diagramme entité association (ERD). Le mieux pour commencer est de formuler le problème à modéliser, le contenu de la base sous forme de **texte libre**. pour cette étape il est nécessaire de bénéficier de l'aide des experts du domaine concerné et de travailler de manière collective en échangeant.

Les étapes utilisées sont :

1. Identifier les entités / objets de la base
2. Identifier ceux qui seront les tables (entités) et d'autres des attributs de ces tables (colonnes)
3. Identifier les relations entre les entités
4. Identifier les cardinalités des relations

## Travaux pratiques

### Installation / Lancement de mysql ou postgresql

***

## Manipulations de bases

L'acronyme **CRUD** (pour create, read, update, delete) (parfois appelé **SCRUD** avec un "S" pour search) désigne les quatre opérations de base pour la persistance des données, en particulier le stockage d'informations en base de données.

## Vocabulaire

| Terme       | Définition                                                |
| ----------- | --------------------------------------------------------- |
| projection  | Sélection d'attributs d'une table (colonnes)              |
| restriction | Sélection de lignes d'une table (nécessite une condition) |
| MPD         | Modèle de données                                         |
| SGBD        | système de gestion de base de données                     |

## Exemples de BDD

Des bases de données sont disponibles sur [le site mysql-exemples](https://dev.mysql.com/doc/index-other.html)

Données ouvertes du gouvernement :

- Fichier jointure de données (avec des redondances) [Data.gouv](https://www.data.gouv.fr/fr/)

## TP

- Pour débuter utiliser XAMPP et PHPmyAdmin
- [Buzut, utiliser MySQL en CLI](https://buzut.net/maitrisez-mysql-en-cli/)

Exemple de création d'une table avec clef étrangère en MySQL.

```sql
CREATE TABLE Orders (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    PersonID int,
    PRIMARY KEY (OrderID),
    FOREIGN KEY (PersonID) REFERENCES Persons(PersonID)
);
```

## Ressources

- Cours Khan Academy : [Introduction au SQL](https://fr.khanacademy.org/computing/computer-programming/sql) : Cours plus défis (TP, projets) en ligne.