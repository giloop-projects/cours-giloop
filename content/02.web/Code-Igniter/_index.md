---
title: CodeIgniter
weight: 20
chapter: false
tags: [php, codeigniter]
---


## Présentation

CodeIgniter est un framework PHP qui se veut léger et donc plus rapide que Symfony. 

Comme la majorité des frameworks PHP il suit l'architecture **MVC : Modèle Vue Contrôleur**, mais pour des raisons de simplicité on peut se contenter d'utiliser les blocs Vue et Contrôleur, le contrôleur assure la logique de gestion des données.

## Code Igniter 3

### Quick start

- Télécharger le [zip du framework](https://codeigniter.com/userguide3/installation/downloads.html)
- Dézipper dans un dossier de votre 
- Suivez le [tutorial](https://codeigniter.com/userguide3/tutorial/index.html) pour créer un site de news

Code SQL pour insérer des news dans la table `news` : 

```sql
INSERT INTO `news` (`id`, `title`, `slug`, `text`) VALUES (NULL, 'Bonne année', 'bonne-annee', 'Vive 2021, santé bonheur à tous');
INSERT INTO `news` (`id`, `title`, `slug`, `text`) VALUES (NULL, 'Trump Out', 'trump-out', 'Adieu Trump, vive Biden') ;
```

Les sections ci-dessous sont juste des notes par rapport au tutorial proposé dans la doc. 

### Création d'une page statique

- Créer un contrôleur dans `application/controllers`
- Créer une vue (template) dans  `application/views`
- Créer la route pour accéder à la page

### Création d'une page dynamique

- Créer le modèle dans `application/models`
- Créer un contrôleur dans `application/controllers`
- Créer une vue (template) dans  `application/views`
- Créer la route pour accéder à la page

### Rewrite URL serveur apache

Par rapport au tutorial, pour supprimer la partie `index.php` présent dans toute les routes, il faut ajouter une directive au serveur. Si c'est un serveur Apache :

```apache
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)$ index.php/$1 [L]
</IfModule>
```

[Explications rewrite apache](https://codeigniter.com/userguide3/general/urls.html#removing-the-index-php-file) dans la documentation.

### Déploiement sur hébergeur

- Modifier `config.php` → $baseURL
- Modifier les identifiants de connexion `database.php`
- Uploader l'ensemble dossier dans le dossier

## Code Igniter 4

La structure de l'application est assez différente. Aussi un serveur est fourni pour faciliter le développement. A la date de ces notes la version 4.0.4 est assez récente et toutes les librairies de la version 3 n'ont pas encore été portées. 

{{% notice tip %}}
Attention, il se peut que votre antivirus bloque le serveur fournit, il faut dans ce cas ajouter une exception.  
{{% /notice %}}

## Ressources

- Documentation [Code Ingiter 3](https://codeigniter.com/userguide3/index.html)
- Documentation [Code Ingiter 4](https://codeigniter.com/userguide4/index.html)
