---
title: PHP / MySQL
weight: 20
chapter: false
---


## Présentation

PHP est un pré-processeur de HMTL. C'est un programme exécuté sur un serveur qui renvoie du HTML au navigateur. 
C'est aussi le nom donné au langage de programmation utilisé par ce programme.

Le fait d'avoir un langage de programmation pour générer du HTML permet de faire :

- des pages qui vont chercher des données protégées sur le serveur 
- des pages dont le contenu est personnalisé pour un utilisateur
- des pages qui s'interfacent avec d'autres programmes sur le serveur

## Ressources

- Le [site de référence du PHP](https://www.php.net/)
- [Les ressources officielles pour le PHP](https://www.php-fig.org)
- [PSR : PHP Standards Recommendations](https://www.php-fig.org/psr)
