---
author: ["Gilles Gonon"]
title: "Local / hébergeur"
weight: 4
chapter: false
tags: ["dev", "web", "développeur"]
description: "Présentation des étapes pour migrer un projet développé en local chez un hébergeur"
---

## Travailler en local et chez un hébergeur

### Travail en local

Dans un projet web, il est fréquent de travailler en local pour la mise en route du projet

Travailler en local présente de nombreux avantages : 

- rapidité du serveur local, 
- manipulation des fichiers, 
- droits administrateurs, ...

Fatalement, le moment arrivera ou il faudra mettre son travail en ligne pour le rendre accessible aux autres.
Pour cela, il est nécessaire de connaître et maîtriser les étapes pour réussir vos migrations.

Par la suite, ces étapes pourront même être automatisées dans le cadre du déploiement continu, mais c'est une autre histoire ...


### Étape #0 : Prise en main de l'hébergement

- 🧪 Tester la connexion avec FileZilla
- 🔎 Identifier le dossier racine du site
  - `www`, `httpdoc`, `public_html`, ... ?
- Faut-il créer la BDD, ses utilisateurs ?
  - Penser à bien définir les privilèges des utilisateurs 🔏
- Valider les identifiants de la base de données 🛡
- Utiliser un script de test 📝

A minima, le script de test contient les identifiants de la base de données et réalise une connexion. 

```php
// Identifiants
$servername = "localhost";
$dbname = "db";
$username = "user";
$password = "pass";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    echo "<h1>Connexion réussie !</h1>\n";
    $conn = null;
}
catch(PDOException $e)
{
	echo "<h1>Échec de la connexion</h1>\n";
    echo "Connection failed: " . $e->getMessage();
}
?> 
```

### Étape #1 : Export de la BDD

- Création d'un fichier SQL avec PhpMyAdmin
- C'est du texte : il est possible de l'éditer 📝
- Conformité des liens : Rechercher / remplacer

{{% notice warning %}}

La gestion des accents peut être problématique 🔥.  
La recommendation actuelle est d'utiliser le format utf8 / utf8mb4.  
Pour les tables de la base de données, cela correspond à l'encodage `utf8_general_ci` ou `utf8mb4_unicode_ci`.  
Vérifier en local et sur l'hébergeur.

{{% /notice %}}

```php
$conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8mb4", $username, $password);
```

### Étape #2 : Upload des fichiers 🌐 ⤴️

1. Faites une copie en local du dossier 💾
2. Modifiez les infos de configuration 🔑 : connexion à la BDD, ...
3. Envoyer les fichiers avec FileZilla

### Étape #3 : import SQL 📥

- Utiliser PhpMyAdmin côté hébergeur
- 🔧 Créer un script d'import si besoin
- 🔒 Protéger le script ou supprimer le !

{{% notice tip %}}
💡 Utiliser des fichiers sevreur pour gérer l'accès à un dossier :  
 `.htaccess` et `.htpasswd` 
{{% /notice %}}


![Mémo pour la migration des projets locaux vers un hébergeur](/cours-giloop/images/Migration-local-hebergeur-step-3.png)

### Exercices

Le fichier ZIP ci-dessous contient le code source d'une application simple de chat ainsi qu'une base de données associée.

- Faites fonctionner l'application en local (WAMP, MAMP, ...). Pour cela il faut :
  - Modifier le fichier `config.php` avec vos identifiants de base de données
  - Importer les tables dans une base de données
- Créer un compte sur PlanetHoster
- Faites fonctionner l'application sur Créer sur ce compte
- Analyser et tester les scripts d'import / export d'une base SQL en PHP
- Créer une sauvegarde automatique de votre base de données avec une **tache planifiée** `cron`

{{%attachments title="Code source chat PHP avec connexion" pattern=".*(zip)" style="green" /%}}

{{%attachments title="Scripts PHP utilitaires pour importer / exporter en SQL" pattern=".*(php)" style="blue" /%}}
