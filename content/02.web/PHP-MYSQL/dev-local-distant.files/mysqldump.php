<?php

/*
 * Script pour exporter une base de donnees Mysql vers un fichier SQL
 * avec l'utilitaire mysqldump
 */ 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Connexion a la base
$database = 'db';
$user = 'user';
$pass = 'pass';
$host = 'localhost';
$file = dirname(__FILE__) . '/backup-'.date("Y-m-d-His").'sql';

echo "<h3>Backing up database to `<code>{$file}</code>`</h3>";

exec("mysqldump --user={$user} --password={$pass} --host={$host} {$database} --result-file={$file} 2>&1", $output);

var_dump($output);

?>
