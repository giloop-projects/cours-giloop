<?php

/*
 * Script pour exporter une base de donnees Mysql vers un fichier SQL par table
 * avec l'utilitaire mysqldump
 */ 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$database = 'db';
$user = 'user';
$pass = 'pass';
$host = 'localhost';
$date = date("Y-m-d-His");

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
    echo "<h1>Connected successfully</h1>\n";
	
	echo "<h2>Liste de toutes les tables de la base</h2>";

	$sql = "SHOW TABLES";
	echo "<ul>";
	foreach ($conn->query($sql) as $row) {
		echo "<li>Table: {$row[0]}</li>\n";
	}
	echo "</ul>";

	echo "<h2>Liste des tables</h2>";
    echo "<h3>Backing up {$dbname}</h3>";
    
	$sql = "SHOW TABLES like 'is%'";
	// $sql = "SHOW TABLES like 'is%'"; // ajout d'un filtre dans les noms de table
	
	echo "<ul>";
	foreach ($conn->query($sql) as $row) {       
        $file = dirname(__FILE__) . '/backup-'.$date.'-'.$row[0].'.sql';
        echo "<li>Dumping Table: {$row[0]} → {$file}</li>\n";
        exec("mysqldump --user={$user} --password={$pass} --host={$host} {$dbname} --tables {$row[0]} --result-file={$file} 2>&1", $output);
        var_dump($output);
	}
	echo "</ul>";
}
catch(PDOException $e)
{
	echo "<h1>Connection failed</h1>\n";
    echo $e->getMessage();
}

?> 

