<?php
/* 
 * Script PHP pour importer un fichier SQL en base de données lorsque phpMyAdmin n'est pas disponible 
 * ou pour automatiser un traitement.
 */

// Name of the file
$filename = 'base.sql';
// MySQL host
$host = 'localhost';
// MySQL username
$username = 'user';
// MySQL password
$password = 'pass';
// Database name
$database = 'nom_base';

$query_count = 0;

try {
    /* Connexion à la BDD/BD/DB (database)
     * /!\ Attention à l'encodage avec le paramètre charset=utf8mb4
     */
    $dbh = new PDO("mysql:host=$host;dbname=$database;charset=utf8mb4", $username, $password);

    // Ouverture du fichier
    $lines = file($filename) or die("Impossible d'ouvrir le fichier.");

    // Variable temporaire, pour stocker une à une les variables du fichier
    $templine = '';

    echo("<ul>");

    /* Boucle pour lire le fichier ligne par ligne */
    foreach ($lines as $line) {
        // Saute les lignes de commentaires et les lignes vides
        if (substr($line, 0, 2) == '--' || $line == '')
            continue;

        // Concatène les lignes jusqu'à la fin de la requête
        $templine .= $line;

        // Un point-virgule indique la fin de la requête : on exécute la requete
        if (substr(trim($line), -1, 1) == ';') {
            // Requete
            $dbh->query($templine, PDO::FETCH_NUM) or print('Error performing query \'<strong>' . $templine . '\': ' . $con->error() . '<br /><br />');
            // Vide la variable temporaire pour la requete suivante
            $templine = '';
            // Info 
            $query_count++;
            echo("<li>$query_count requetes effectuées.</li>");

        }
    }

    echo "</ul>";
    echo "<p>Les tables ont été importées avec succès</p>";
    
    // Ferme la connexion à la DB
    $dbh = null;

} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

?>