<?php
/*
 * Script PHP miimal pour vérifier la connexion à une BDD
 * Liste les tables de la base
 * Liste également les tables avec un préfixe donné (utile pour Wordpress)
 *
 */

// Identifiants
$servername = "localhost";
$dbname = "db";
$username = "user";
$password = "pass";

$motif = "%user%";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
    echo "<h1>Connexion réussie !</h1>\n";
	
	// Requete 1
	echo "<h2>Liste de toutes les tables de la base</h2>";

	$sql = "SHOW TABLES";
	$result = $conn->query($sql);

	if ($result->rowCount() > 0) {
		echo "<ul>";
		while($row = $result->fetch()) {
			echo "<li>Table: {$row[0]}</li>\n";
		}
		echo "</ul>";
	} else {
		echo "<p>Pas de table dans la base $dbname</p>";
	}
	
	// Requete 2
	echo "<h2>Liste des tables avec le motif <code>$motif</code></h2>";
	
	$stmt = $conn->prepare("SHOW TABLES like :motif");
	// $stmt->bindParam(':motif', $motif, PDO::PARAM_STR);
	$stmt->execute(array(':motif' => $motif));
	
	if ($result->rowCount() > 0) {
		echo "<ul>";
		while($row = $stmt->fetch(PDO::FETCH_NUM)) {
			echo "<li>Table: {$row[0]}</li>\n";
		}
		echo "</ul>";
	} else {
		echo "<p>Pas de table trouvée $sql</p>";
	}
}
catch(PDOException $e)
{
	echo "<h1>Échec de la connexion</h1>\n";
    echo "Connection failed: " . $e->getMessage();
}
?> 

