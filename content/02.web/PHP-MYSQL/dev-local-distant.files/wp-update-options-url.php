<?php

// Utilitaire pour changer les options de Wordpress HOME / URL 

$servername = "localhost";
$dbname = "db";
$username = "user";
$password = "pass";

$home = "http://www.mywordpress.com/my-project";
$url = "http://www.mywordpress.com";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
    echo "<h1>Connected successfully</h1>\n";

    
    $sql = "UPDATE `is_options` SET `option_value`='$home' WHERE `option_id`=1";

    if ($conn->query($sql)) {
        echo "Record 1 updated successfully <br />";
    } else {
        echo "Record not updated<br />";
        print_r($conn->errorInfo());
    }

    $sql = "UPDATE `is_options` SET `option_value`='$url' WHERE `option_id`=2";

    if ($conn->query($sql)) {
        echo "Record 2 updated successfully <br />";
    } else {
        echo "Record not updated<br />";
        print_r($conn->errorInfo());
    }

}
catch(PDOException $e)
{
    echo "Connection failed: " . $e->getMessage();
}
?> 
