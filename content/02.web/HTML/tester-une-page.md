---
author: ["Gilles Gonon"]
title: "Tester un site"
weight: 6
chapter: false
tags: ["dev", "web", "développeur"]
---

## Tester et auditer votre site

### Des tests, des tests et encore des tests

Les sites sont aujourd'hui majoritairement à faire en **Mobile First**.

Pour chacun de vos sites, pour l'ensemble des pages, les points essentiels sont :

1. Vérifier que le site ne contient **pas d'erreur** 👌
2. Le site est **utilisable** : les utilisateurs arrivent facilement à leur but 🤙
3. Vérifier que le site est **rapide** 🚀

🔥 Dans les tests utilisateurs sur le taux de conversion des sites **la vitesse est 3 fois plus importantes que l'aspect visuel** 📈.

On pourra donc se contenter dans la majorité des cas d'utiliser des visuels standards (bootstrap, cards, materialize) qui faciliteront l'utilisabilité du site.

![speed-matters-googleio19.png](/cours-giloop/images/speed-matters-googleio19.png?width=20pc)

### Valider votre site

#### La structure DOM HTML

📋 Une première étape est aussi de vérifier dans Firefox le code source de la page (Ctrl+u ou clic droit → voir le code source de la page) : les erreurs de syntaxe, balise non fermée apparaissent en rouge.

📋 Faire une vérification syntaxique de votre code HTML sur le site [validator.w3.org](https://validator.w3.org/) → corriger toutes les erreurs que vous indique le parser syntaxique. 🧐

📋 Vous pouvez aussi faire une validation plus poussée de la structure de vos données avec le site [Structured Data linter](http://linter.structured-data.org/)

#### Rendu visuel

Une fois la structure sans erreur, il est important de valider le rendu visuel sur les principaux navigateurs :

- Commencer par le **mobile** : Android (Chrome, Firefox, Opera) et iOS (Safari)
- Windows (Edge, Internet Explorer, Chrome, Firefox, Opéra, ...)
- Mac OS : Safari
- Linux : Chrome, Firefox

Cette étape doit permettre de valider le rendu visuel du contenu réalisé par le CSS.

Vérifier également l'aspect **responsive** du site en utilisant l'inspecteur du navigateur ou réduisant simplement la largeur de la fenêtre.

#### Valider les liens

Il est important aussi de valider que votre site ne contient pas de liens cassés.

L'outil d'analyse recommandé et très puissant est [Xenu's link sleuth](http://home.snafu.de/tilman/xenulink.html). Voir aussi [un guide sur SeoMix](https://www.seomix.fr/xenu/).

#### Vérifier la console Javascript

La console Javascript est votre amie. Elle vous indiquera de nombreux messages sur les programmes javascript en cours d'exécution. Notamment, pour la validation, filtrer uniquement les messages **erreurs** et **avertissements**.

![imagesconsole-warn-err.png](/cours-giloop/images/console-warn-err.png)
🔥 il n'est pas toujours possible de corriger les warnings ou les erreurs suivants les librairies utilisées. Dans ce cas, il faut bien s'assurer que le site reste fonctionnel

### 🎓 L'audit : Lighthouse et les autres

#### Lighthouse

[L'extension Lighthouse](https://chrome.google.com/webstore/) de Google fournit une analyse très complète de vos pages. Les critères analysés sont expliqués en détail [ici](https://developers.google.com/web/tools/lighthouse/v3/scoring) :

- **Performances** : indique le temps de chargement de votre page ainsi que l'utilisation du CPU qui peut être problématique avec Javascript.
  - score > 90 : site rapide 😎
  - score > 50 : site moyen 😕
  - score <50 : site lent 💩
- **Accessibilité** : Légalement tous les sites web et applications mobiles des organismes publics devront bientôt être accessibles (09/2020). Voir [la loi](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016L2102&from=EN#d1e40-1-1) ou [un résumé](https://anysurfer.be/fr/blog/detail/la-directive-europeenne-relative-a-laccessibilte-des-sites-web-resume). Voir également les guides : [vue d’ensemble des standards d’accessibilité du W3C](https://www.w3.org/WAI/standards-guidelines/fr) et [Principes d’accessibilité](https://www.w3.org/WAI/fundamentals/accessibility-principles/fr).
- **Bonnes pratiques** : les bonnes pratiques web sont en constantes évolutions, et les points audités par ce critère sont donc intéressant à suivre.
- **SEO** : les critères SEO à vérifier sont assez simples à mettre en place, il faut viser les 100%. Il ne correspondent à une stratégie SEO mais garantissent un référencement naturel de la page qui est assez complet.
- **PWA** : analyse des critères pour faire de votre site une PWA.

![Exemple de résultat d'audit Lighthouse](/cours-giloop/images/ex-lighthouse-report.png)

Voir le guide d'utilisation de [Lighthouse](https://developers.google.com/web/tools/lighthouse/)

💡 Il est important de lire l'ensemble des conseils fournis. Tous ne pourront pas être appliqués, et vous ne les comprendrez pas tous.

> Une chose est sure : il vous permettrons tous de progresser ❣
> 📆 Creuser un nouvel aspect à chaque audit de site

📣  Utiliser Google Chrome pour lancer l'audit, dans l'onglet ``Audits`` des DevTools

##### Remarques sur les images

Systématiquement Lighthouse vous suggère d'encoder les images au format WebP. Le problème est que ce format [n'est pas supporté](https://caniuse.com/#search=webp) pas tous les navigateurs (notamment Safari et IE), au moment de l'écriture de cet article.

Pour l'utiliser, il faut donc encore proposer l'alternative en PNG/JPG. Pour cela utiliser les balises suivantes dans votre code pour déclarer les images :

```html
<picture>
	  <source srcset="/cours-giloop/images/logo.webp" type="image/webp">
	  <img src="/cours-giloop/images/logo.png" id="logo" alt="IT Nota Logo example">
</picture>
```

🔎 L'outil [cwebp](https://developers.google.com/speed/webp/) permet de convertir les images en ligne de commande.

🔎 Un outil efficace de réduction de taille de vos images est [TinyPNG](https://tinypng.com/) qui ofre de bons taux de compression sans perte de qualité.

##### Politique de cache et autres infos

Des explications très claires sur [developers.google.com](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/http-caching#defining-optimal-cache-control-policy). Dans le cadre d'un ensemble de guides sur les fondamentaux du web.

#### L'audit de rapidité : PageSpeed

L'outil [PageSpeed Insight](https://developers.google.com/speed/pagespeed/insights/) permet de tester plus spécifiquement la rapidité d'une page.

#### Les DevTools du navigateur

Firefox (Developper Edition) et Chrome contiennent des outils avancés pour suivre les performances d'une page.
Ils incluent notamment :

- l'inspecteur pour vérifier le code de la page générés et les styles CSS,
- des outils d'analyse/édition en ligne des styles (propriétés CSS, grilles, courbes d'animations, ...)
- un débogueur JS
- un profileur de performances pour tracer aussi les étapes du chargement d'une page
- un analyseur de requêtes réseaux : donne à la fois les performances et les infos d'entêtes HTTP. Il permet aussi de simuler des réseaux lents (GPRS, 2G, 3G, ...)
- les infos de stockage : cookies, local storage et indexed db
- les infos d'accessibilités de la page

Pour Firefox, les infos sont sur [MDN developper tools](https://developer.mozilla.org/fr/docs/Outils).

#### D'autres outils

🔖 Un article intéressant sur [la Fabrique du Net](https://www.lafabriquedunet.fr/blog/audit-site-web-fondamentaux/) présentant les fondamentaux de l'audit d'une page web sous différents aspects, notamment le e-commerce.

Les audits LightHouse et validator.w3.org donnent des indications, mais sont loin de fournir toutes les analyses, notamment aux niveaux rendu responsive, orthographe, pertinence du contenu et usabilité.

Il existe aussi d'autres outils d'audits en ligne mentionnés dans l'article :

- https://www.dareboost.com/ : l'analyse gratuite est complète et donne des infos détaillées en français (5 analyses gratuites par mois)
- https://www.webpagetest.org/ : permet de tester la mise en cache (la page est rechargée 3 fois)
- https://tools.pingdom.com/
- https://www.boia.org/products/free-wcag-2-0-aa-report/ : le Bureau of internet accessibility fournit un outil gratuit d'analyse d'accessibilté de vos sites et de nombreuses infos. 

### 📖 L'orthographe : c'est vous qu'on juge !

> Quand vous faites une faute, on ne juge pas votre orthographe, mais on Vous juge 💘

- L'orthographe : toute une histoire, pas glorieuse mais importante pour partager le sens
- À regarder : [La faute de l'orthographe](https://youtu.be/5YO7Vg1ByA8)

→ 💡 Reliser vous, Faites vous **relire**

### Faire des tests utilisateurs

Votre premier utilisateur sera sans doute votre client. Il n'est pas forcément lui-même son client et peut avoir un avis subjectif biaisé focalisé sur l'esthétique la couleur des boutons, les logos, ...

Il est donc important de faire des tests avec de vrais utilisateurs cibles.

À défaut, faites tester le site à vos amis, aux autres corsaires. Essayer de leur donner une tâche précise à accomplir sur le site, un **scénario utilisateur**, pour aller au-delà d'une navigation juste esthétique.

On parle beaucoup maintenant du **Call To Action**  pour parler des incitations utilisateurs dans leur navigations.

## Faire des pages allégées

Le gros du téléchargement d'une page web provient des images ou vidéos incluses dans la page.

Il faut traiter les images en 2 étapes :

- Redimensionner les images au plus juste par rapport à la taille maximale affichée dans la page : utiliser notamment des vignettes pour les images qui sont des liens vers d'autres projets
- Possibilité d'utiliser 2 images de tailles différentes en fonction de la taille de l'écran, en css avec les **media queries** : _@media_

```html
<link rel="stylesheet" media="screen and (min-width: 900px)" href="widescreen.css">
<link rel="stylesheet" media="screen and (max-width: 600px)" href="smallscreen.css">
```

- Sauver les photos en JPEG avec un taux de compression adapté :
  - 80% pour des vignettes
  - 85% pour des images
  - il n'est jamais nécessaire pour un écran d'aller au-delà de 90% de compression (les taux de compression > 90% sont nécessaires pour des tirages photos avec agrandissement)

**Mobile First : La tendance actuelle est de faire le design d'abord pour le téléphone !**

Voir les résolutions standard de téléphone, tablettes et PC, notamment dans ce très bon tutoriel de w3schools sur les [media query](https://www.w3schools.com/css/css_rwd_mediaqueries.asp).

## Pour aller plus loin

- La [documentation de Lighthouse](https://developers.google.com/web/tools/lighthouse), où l'on trouve beaucoup d'explication sur les tests
- Les indicateurs de tests sont en constante évolution, et sont présentés sur le site [Web Dev Vitals](https://web.dev/vitals/).
- La CNIL propose un [kit de bonnes pratiques](https://www.cnil.fr/fr/developpeurs-la-cnil-met-en-ligne-un-kit-de-bonnes-pratiques) pour les développeurs, à lire !
Au delà de documents électroniques le Web est devenu la plateforme qui fournit les aplications les plus avancées. http://testthewebforward.org/ propose une initiative de réflexion autour des tests des plateformes.

{{< youtube id="mLjxXPHuIJo" width="50%" >}}

## Liste de tests

1. CSS validation
2. HTML or XHTML validation
3. Page validations with and without JavaScript enabled
4. Ajax and JQeury functionality
5. Font size validation
6. Page layout in different resolutions
7. All images and alignment
8. Header and footer sections
9. Page content alignment to center, LHS or RHS
10. Page styles
11. Date formats
12. Special characters with HTML character encoding
13. Page zoom-in and zoom-out functionality

And obviously, you will have to repeat these tests on:
14. Different Operating Systems like Windows, Linux, and Mac
15. Different browsers (with different versions) like Internet Explorer, Firefox, Google Chrome, Safari, and Opera.