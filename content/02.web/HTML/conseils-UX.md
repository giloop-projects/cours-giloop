---
title: "Quelques conseils d'UX"
author: "Gilles Gonon"
weight: 20
chapter: false
tags: ["html", "web", "ux"]
---

## Quelques conseils d'UX

Voici des infographies sur l'UX glanées sur internet :

- [11 lois de l'UX](https://www.impactbnd.com/blog/laws-of-ux-infographic)

![Toptal - 11 lois de l'UX](/cours-giloop/images/toptal-laws-of-UX.png)

- [30 statistiques éclairantes sur l'UX](https://www.impactbnd.com/blog/user-experience-stats-infographic)
![30-stats-ux.png](/cours-giloop/images/30-stats-ux.png)

- [Quelques tendances design en 2020](https://www.impactbnd.com/blog/trends-in-design-in-2020)
