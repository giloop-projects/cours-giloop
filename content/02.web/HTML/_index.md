---
title: HTML
weight: 1
chapter: false
---


## À la découverte du HTML

**HTML** est le langage de base des pages WEB, interprété par l'ensemble des navigateurs Web. C'est un langage à partir de balises (Markup) qui permet de faire des liens HyperText entre les pages : voilà ce qu'est le **HyperText Markup Language**.

C'est une instance du langage plus générique qu'est **XML** (eXtended Markup Language), c'est-à-dire qu'il en suit les règles syntaxiques avec son propre vocabulaire de balises prédéfinies. 
