---
title: "Les outils"
author: "Gilles Gonon"
weight: 2
chapter: false
tags: ["html", "web", "code"]
---

## Environnement de travail

### L'éditeur pour écrire le texte

Nous allons travailler avec  **Visual Studio Code** :heart: pour éditer les pages HTML.  
Les extensions nécessaires pour le web sont :

- HTMLHint : validation du code
- Live server
- Open HTML in default browser : raccourci clavier **``Ctrl+&``** dans l'éditeur d'une page html.

### Le navigateur pour analyser les pages et déboguer

Nous allons travailler avec **Firefox** et **Chrome**.  

Installer l'extension LightHouse report generator.

- [Extension lighthouse pour Firefox](https://addons.mozilla.org/fr/firefox/addon/lighthouse-report-generator/)
- [Extension lighthouse pour Chrome](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk?hl=fr)

![lighthouse-icon.png](/cours-giloop/images/lighthouse-icon.png)

Cette extension permet de générer un audit d'un site ou d'une page sur différents aspects. Tester le sur [un site](http://institutsolacroup.com/) de votre choix.  

>Dans les évaluations de vos projets nous vous  demanderons un score supérieur à 80 %

Lors de la navigation, les pages sont téléchargées côté client et il est possible d'analyser le code HTML. Pour cela faire un clic droit sur une page et ``... code source de la page`` (raccourci clavier **``Ctrl+u``**)

### Liste d'outils

Une [liste plus complète d'outils](https://developer.mozilla.org/fr/docs/Apprendre/Commencer_avec_le_web/Installation_outils_de_base) pour l'ensemble du travail Web est disponible sur le site de Mozilla developper.

## Partir de la page blanche

Dans Visual Studio Code, il faut d'abord créer un nouveau dossier pour votre site. Ouvrir le dossier dans Visual Studio Code (menu File → Open Folder ... ou ``Ctrl+K Ctrl+O`` pour ouvrir un dossier de travail).  
![Créer un fichier vide dans Studio code](/cours-giloop/images/creer-fichier-studio-code.png)

Nous allons pré-remplir les balises HTML dans des fichiers vides à partir de 2 méthodes :

1. Créez 1 fichier vide **test-1.html** et tapez ``doc`` ou ``html5`` au début du document, cliquer sur le menu doc. 
![emmet-doc-html.png](/cours-giloop/images/emmet-doc-html.png)

2. Créez 1 autre fichier vide **test-2.html** et tapez ``Ctrl+espace`` au début du document puis choisir le menu ``HTML sample``
![html-starting-point.png](/cours-giloop/images/html-starting-point.png)

> Quelles sont les différences entre les 2 pages créées ?

Ces squelettes seront souvent notre point de départ pour créer de nouvelles pages HTML. 

## Apprendre le HTML

Suivre le module _Introduction au HTML_ de la formation de Mozilla developper : [Commencer avec le HTML](https://developer.mozilla.org/fr/docs/Apprendre/HTML/Introduction_%C3%A0_HTML)  

## Création d'une page guidée

Voici ci-dessous un aperçu de la page à créer. Les ressources et un squelette de la page sont disponibles dans les ressources du NAS : \\Solacroup_ecole\ressources\Cours\HTML\PageACoder.zip

Le fichier zip contient les dossiers pour organiser les éléments de la page, ainsi que la capture d'écran du site : 
![contenu-dossier-zip.png](/cours-giloop/images/contenu-dossier-zip.png)

La feuille de style css à utiliser ``styles.css`` est dans le dossier design et ne contient que le code pour appeler la fonte dans le titre 1.  

![Page d'un site de Vans a créer](/cours-giloop/images/Site-Kevin-a-creer.jpg)

Ajouter un effet css sur l'élément **hover** du bloc de modèle de van pour modifier la taille de l'ombre au passage de la souris.

![effet-css-hover-cartevan.png](/cours-giloop/images/effet-css-hover-cartevan.png)

## Le DOM

A compléter.
