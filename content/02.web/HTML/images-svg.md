---
title: "Images SVG"
author: "Gilles Gonon"
weight: 7
chapter: false
tags: ["html", "web", "images", "svg"]
---

## Travailler avec des images SVG

### Le format SVG

Notes extraites du [tutoriel MDN](https://developer.mozilla.org/fr/docs/Web/SVG/Tutoriel/Introduction)

SVG, pour Scalable Vector Graphics (ou encore Graphismes vectoriels redimensionnables), est un langage basé sur le XML du W3C qui permet de définir des éléments graphiques avec des balises.

L'intérêt d'un format vectoriel est que l'image peut être manipulée sans perte de qualité (agrandissement, déformation, couleurs, ...). Une image SVG est souvent aussi plus légère que son rendu matriciel (export au format PNG ou JPG).

Celà est d'autant plus intéressant dans le contexte du Web car le contenu d'un fichier SVG est directement accessible via le DOM et peut être modifié avec du CSS ou du Javascript.

Le format est pris en charge par les [principaux navigateurs](https://caniuse.com/#feat=mdn-svg_elements_svg).

### Méthode 1 : inclusion du code SVG dans des balises <svg>

Le code du fichier SVG ci-dessous est collé directement dans le document à l'endroit souhaité. Il s'agit du logo de la fondation avec 3 carrés de couleur et 3 zones de texte :

```svg
<svg id="logo-imts" width="183" height="80" version="1.1" viewBox="0 0 183 80" xmlns="http://www.w3.org/2000/svg">
    <rect id="carre-vert" x=".35296" y="15.171" width="14.086" height="15.277" fill="#8ebe1c"/>
    <rect id="carre-rose" x="22.362" y="26.305" width="14.086" height="14.966" fill="#e7007c"/>
    <rect id="carre-bleu" x="14.439" y=".25694" width="14.137" height="14.914" fill="#046db4"/>
    <text id="txt-ins" transform="scale(.9807 1.0197)" x="46.699856" y="40.854664" font-family="sans-serif" font-size="15.022px" stroke-width=".72522" style="line-height:1.25" xml:space="preserve">INSTITUT</text>
    <text id="txt-mt" transform="scale(.99205 1.008)" x="46.423439" y="60.651131" font-family="Candara" font-size="19.821px" font-weight="bold" stroke-width=".95688" style="line-height:1.25" xml:space="preserve">MARIE-THÉRÈSE</text>
    <text id="txt-sol" transform="scale(.9802 1.0202)" x="46.431408" y="78.290649" font-family="sans-serif" font-size="19.674px" stroke-width=".94881" style="line-height:1.25" xml:space="preserve">SOLACROUP</text>
</svg>
```

Les éléments du fichier sont directement accessibles avec les sélecteurs CSS. Par exemple : 

### Méthode 2 : inclusion d'un fichier SVG dans une balise <object>

Si le rendu SVG n'est pas disponible, la balise `object` permet d'inclure un fallback (solution de repli), comme par exemple un texte :

```html
<object id="logo-object" data="logo_opt.svg" type="image/svg+xml" width="250px" >
    SVG n'est pas pris en compte par votre navigateur, désolé.
</object>
```

Ci-dessous, un exemple avec fallback dans une balise `img` avec un format supporté d'image.

```html
<object id="logo-object" data="logo_opt.svg" type="image/svg+xml" width="250px" >
    <img src="logo_opt.png" alt="Logo Institut Marie-Thérèse Solacroup">
</object>
```

Le contenu du fichier est accessible comme arbre DOM via la propriété `contentDocument` de l'élément `object`. Il est ensuite possible de modifier chaque élément avec CSS ou en javascript. 

```javascript
var contenu_svg = document.getElementById('logo-object').contentDocument
```

### Méthode 3 : inclusion d'un fichier SVG dans une balise <img>

```html
<img src="logo_opt.svg" alt="Logo Institut Marie-Thérèse Solacroup" />
```

L'élément se comporte alors comme une image, ses propriétés ne sont pas accessibles dans le DOM.

Cette méthode ne présente pas grand intérêt si ce n'est de modifier à minima le code de vos pages si vous modifiez le format de certaines images.

### Bonus 1 : animation sur SVG inclus en texte

Soit l'image SVG suivante, incluse dans votre page via la méthode 1 :

```svg
<svg id="logo-imts-anim" width="183" height="80" version="1.1" viewBox="0 0 183 80" xmlns="http://www.w3.org/2000/svg">
    <rect id="carre-vert" class="vert" x=".35" y="15.17" width="14.09" height="15.28" />
    <rect id="carre-rose" class="rose" x="22.36" y="26.31" width="14.09" height="14.97" />
    <rect id="carre-bleu" class="bleu" x="14.44" y=".26" width="14.14" height="14.91" />
        <text id="txt-mt" transform="scale(.99 1.01)" x="46.42" y="60.65" font-family="Candara" font-size="19.8px" font-weight="bold" stroke-width=".96" style="line-height:1.25" xml:space="preserve">MARIE-THÉRÈSE</text>
    <text id="txt-ins" transform="scale(.98 1.02)" x="46.7" y="40.85" font-family="sans-serif" font-size="15px" stroke-width=".73" style="line-height:1.25" xml:space="preserve">INSTITUT</text>
    <text id="txt-sol" transform="scale(.98 1.02)" x="46.43" y="78.29" font-family="sans-serif" font-size="19.6px" stroke-width=".95" style="line-height:1.25" xml:space="preserve">SOLACROUP</text>
</svg>
```

Le code Javascript ci-dessous permet de changer la couleur des carrés via des styles CSS prédéfinis :

```css
/* Utilisation de classes de couleur pour animation Javascript */
.vert {fill:#8ebe1c; transition: 1s;}
.rose {fill:#e7007c; transition: 1s;}
.bleu {fill:#046db4; transition: 1s;}
```

```javascript
// Récupération de l'élément correspondant au logo
var el = document.getElementById('logo-imts');
el.onmouseover = logoOver; // Fonction lancée au survol de la souris
el.onmouseout = logoOut; //  Fonction lancée à la sortie de la souris

function logoOver() {
    // Survol de souris : Modifie des classes de styles des éléments 
    el.getElementById('carre-vert').classList.remove('vert');
    el.getElementById('carre-rose').classList.remove('rose');
    el.getElementById('carre-bleu').classList.remove('bleu');
    el.getElementById('carre-vert').classList.add('bleu');
    el.getElementById('carre-rose').classList.add('vert');
    el.getElementById('carre-bleu').classList.add('rose');
}

function logoOut() {
    // Sortie de souris : Restaure les classes de styles des éléments 
    el.getElementById('carre-vert').classList.remove('bleu');
    el.getElementById('carre-rose').classList.remove('vert');
    el.getElementById('carre-bleu').classList.remove('rose');
    el.getElementById('carre-vert').classList.add('vert');
    el.getElementById('carre-rose').classList.add('rose');
    el.getElementById('carre-bleu').classList.add('bleu');
}
```

### Bonus 2 : Création des fichiers SVG avec Inkscape

[Inkscape](https://inkscape.org/) est un outil puissant et gratuit pour la création d'images vectorielles (équivalent gratuit d'Illustrator).  

Il permet notamment de créer des logos assez facilement, mais les fichiers SVG créés ne sont pas optimisés pour le web car ils contiennent beaucoup de méta-information utilisées par l'éditeur. 

Pour préparer vos images SVG pour le web, voici quelques conseils :

- **Donner des id** aux éléments auxquels vous souhaitez accéder en CSS : faire un clic droit → Propriétés de l'objet ...

![Définir la propriété d'un objet dans Inkscape](/cours-giloop/images/inkscape-object-id.png)

- **Dégrouper** l'ensemble des objets et déplacer les éléments pour supprimer les transofmrations inutiles dans le fichier final
- Une fois l'ensemble des éléments positionnés correctement dans votre image, **Grouper** les éléments que vous souahitez manipuler ensemble en CSS et donnez leur une ID
- Une fois le graphique prêt, enregistrer une copie du fichier sous le format "**SVG optimisé**". Cela enlève toutes les metadonnées Inkscape et allège le fichier
- Vous pouvez alors ouvrir le fichier dans un éditeur texte et recopier le code ```<svg>``` dans votre page html

## Exemples

Une version zip des exercices est téléchargeable ci-dessous. Cette version est un instantanné, une version plus à jour est disponible sur un dépôt GIT. 

```bash
git clone https://gitlab.com/imts/images-svg .
```

{{%attachments title="Exemples SVG/HTML/CSS/JS" pattern=".*" style="green" /%}}

## Ressources

- [Documentation Mozilla](https://developer.mozilla.org/fr/docs/Web/SVG/Tutoriel)
- [Effets SVG](http://www.petercollingridge.co.uk/tutorials/svg/interactive/mouseover-effects/)
- [Présentation sur l'animation CSS / SVG (anglais)](https://slides.com/sarasoueidan/styling-animating-svgs-with-css--2#/) : très bonne présentation de [Sara Soueidan](http://sarasoueidan.com/)
- [Les animations SVG avec Inkscape](https://inkscape.org/fr/apprendre/animation/) : pleins de liens intéressants
- [Spécification SVG du W3C](https://www.w3.org/Graphics/SVG/) : document énorme.
