---
author: ["Gilles Gonon"]
title: "Bonnes pratiques"
weight: 4
chapter: false
tags: ["dev", "web", "développeur"]
---

## Projet Web, bonnes pratiques et autres histoires de casse

### L'environnement de travail

Pour chaque nouveau projet, site, ou programme, il est impératif de **travailler dans un nouveau dossier**.

{{% notice note %}}
1 projet Web = 1 dossier
{{% /notice %}}

L'idéal est d'avoir un dossier de développement à la racine de votre ordinateur, avec un chemin respectant les contraintes suivantes :

- pas d'espace,
- pas d'accent,
- pas de caractères spéciaux :`()`, `[]`, `'`, `"`, `#`, `~`, `&`, `{}`, `|`, `\`, `@`, ...:
- Les caractères autorisés sont : [0-9], [a-z], [A-Z], `-`, `_`

{{% notice tip %}}
On parle de **slug** pour désigner le formattage d'une url en miniscule, sans espace et des tirets entre les mots.
L'URL de cette page "Bonnes Pratiques" devient : /bonnes-pratiques
{{% /notice %}}

Par exemple :

- Sous Windows : travailler dans `C:\Dev\Web\`
  - Pour le portfolio `C:\Dev\Web\Portfolio`
  - Pour le wordpress de l'imts : `C:\Dev\Web\wordpress_imts`
  - ...

- Sous Linux /Mac OS : travailler dans le dossier home de l'utilisateur : `/home/nom_user/dev/web` 
  - Pour le portfolio `/home/nom_user/dev/web/Projet_1`
  - Pour le wordpress de l'imts : `/home/nom_user/dev/web/wordpress_imts`
  - ...

### Les noms et chemins de fichiers

#### Sensibilité à la casse

La _casse_ correspond à la différenciation entre lettres **minuscules** et **majuscules**. On parle de sensibilité à la casse. À retenir :

| INSENSIBLE à la Casse | SENSIBLES à la Casse |
|-----------------------|----------------------|
| Mail                  | variables            |
| nom de domaine        | fichiers             |

#### Noms et lien des fichiers

Pour les noms de pages HTML, il est fortement recommandé, d'utiliser des noms de fichiers html en minuscule, sans espace et sans caractères spéciaux.  Avez [a-z], [0-9], -, _

Pour les images, faire au mieux, mais lorsqu'un utilisateur envoie son image, il faut de toute façon traiter le nom, dont on ne gère pas la conformité. 

Lorsque vos fichiers contiennent des caractères spéciaux, il faut utiliser une fonction du type [encodeURI()](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/encodeURI#Description) pour transformer la chaîne en .

- URL : uniform ressources locator;
- URI : uniform ressources identifier;

#### Le fichier index.html

La page **index.html** est la page appelée par défaut lorsque vous indiquez le nom d'un dossier. **C'est donc le nom obligatoire pour votre page d'accueil**.

Lorsque vous utiliserez PHP, la page d'accueil peut aussi s'appeler **index.php**.

#### Chemins des fichiers et dossiers

Il existe trois manières d'indiquer le chemin d'un fichier dans une page web :

- en indiquant le **chemin relatif à la page actuelle** :
  - `<a href="page2.html">Lien vers une page du même dossier</a>`
  - `<a href="./page2.html">Lien vers une page du même dossier</a>`
  - `<a href="articles/article1.html">Lien vers une page dans le dossier articles</a>`
  - `<a href="./articles/article1.html">Lien vers une page dans le dossier articles</a>`
  - `<a href="../index.html">Lien vers une page dans un dossier plus haut</a>`
  - `<a href="../../index.html">Lien vers une page située 2 dossiers plus haut</a>`
- en indiquant le **chemin relatif à la racine du site web**. _C'est quoi la racine du site web ?_ :
  - `<a href="/index.html">Lien vers l'accueil du site</a>`
- en indiquant le **chemin absolu** du fichier : cette option est fortement déconseillée car le chemin absolu d'un fichier n'est valable que sur la machine où il se trouve. Cela peut être nécessaire si vous travaillez directement sur un serveur (et même dans ce cas, on préfèrera un appel en relatif). 
  - `<a href="C:\\Dev\\Web\\Portfolio\\settings.json">Lien vers l'accueil du site</a>`

#### Migration vers un serveur

Côté serveur web, si vous avez respecté ces bonnes pratiques, vous n'aurez souvent qu'à recopier le contenu de votre dossier en ligne puor obtenir l'URL souhaitée.

#### Privilégier HTTPS 🛡️

La plupart des hébergeurs proposent par défaut le protocole sécurisé HTTPS. Il est donc important de mettre une redirection à la racine de votre serveur pour ne jamais pouvoir ouvrir votre site en HTTP. 

🔥Lorsque vous utilisez HTTPS, il est alors nécessaire de référencer tous les inclusions externes au site en HTTPS : librairies JS, framework CSS, Google fontes, ...

Il s'agit d'un fichier de directives serveur (Apache, Nginx, IIS, ...), souvent donné par l'hébergeur lui-même. Par exemple, sur le site gueulomaton.org, le fichier de directive est ``.htaccess`` et contient les informations suivantes: 

```shell
RewriteEngine On
RewriteCond %{SERVER_PORT} 80
RewriteRule ^(.*)$ https://gueulomaton.org/$1 [R,L]
```

## Ressources

Les checklists Opquast pour un meilleur Web. Plein de recommandations (sans solution), avec des filtres par catégories : 

- [Checklist Qualité Web](https://checklists.opquast.com/fr/qualiteweb/)



