---
title: "Appel d'API web"
weight: 2
author: [Gilles Gonon]
tags: [api, interface, rest, graphql]
chapter: false
---

## Présentation

**HTML** et **CSS** permettent la mise en page de contenu web ainsi que des animations de transition.  
Le langage **Javascript** va vous permettre de rendre vos pages dynamiques en modifiant le contenu d'une page lors d'évènements choisis. Une fois les principes d'appel d'un script Javascript assimilés, il est possible d'appeler des fonctionnalités très complexes via une multitude de librairies ([jquery.js](https://learn.jquery.com/), [D3.js](https://d3js.org/), [Parsleyjs](http://parsleyjs.org/doc/examples.html), [Three.js](https://threejs.org/), ...)

Vous allez également pouvoir récupérer des données extérieures depuis d'autres sites via l'appel d'**API**. Il existe des API dans de nombreux domaines, comme le montre le moteur de recherche du [site ProgrammableWeb](https://www.programmableweb.com/category/all/apis).

## Les technologies d'API

Section à faire/compléter

- Les API de type REST (Representationnal State Transfert) sont les plus courantes pour le Web. L'utilisateur effectue une requête http pour obtenir l'info qu'il souhaite. Souvent une requête par type de données. Elles sont conçues avec des cas d'usages en tête. Utilisées depuis près de 20 ans, très répandues, elles répondent au besoins de performances et scalabilité des sites.
- Les API de type GraphQL offrent un cadre plus souple d'accès au données, l'utilisateur choisit les données qu'il veut récupérer, c'est lui qui adapte ses requêtes à ses cas d'usage. Elles sont pour l'instant moins utilisées que les API REST mais leur souplesse d'accès atirent un bon nombre de développeurs (notamment Front End).
- Les API SOAP (Simple Object Acces Protocol)

## Ressources

- [Liste d'API sur Programmable Web](https://www.programmableweb.com/apis/directory)
- [liste d'API graphQL](http://apis.guru/graphql-apis/)
- Article sur le [blog de shevaRezo](https://blog.shevarezo.fr/post/2015/03/17/10-apis-gratuites-utiles) : 10 APIs gratuites utiles pour vos développements

## Infos sur des API

### luftdaten

Le site Luftdaten rassemble des données de pollution atmoshpérique au particule PM2.5 et PM10.
Exemples d'appels :

- Pour un capteur dont on connait l'ID : https://data.sensor.community/airrohr/v1/sensor/23510/
- Pour faire des requêtes avec filtre: [http://api.luftdaten.info/v1/filter/{query}](<http://api.luftdaten.info/v1/filter/%7Bquery%7D>) : all measurements of the last 5 minutes filtered by query
  - type={sensor type} : comma separated list of sensor types, i.e. 'SDS011,DHT22, BME280'
  - area={lat,lon,distance} : all sensors within a max. radius
  - box={lat1,lon1,lat2,lon2} : all sensors in a 'box' with the given coordinates
  - country={country code} : i.e. 'BE, DE, NL, ...'

- Par exemple pour lister des capteurs prêts de moins de 5km d'une position GPS :lat 48.63, long -2.06
[https://data.sensor.community/airrohr/v1/filter/area=48.63,-2.06,5](https://data.sensor.community/airrohr/v1/filter/area=48.63,-2.06,5)
- Les capteurs de type SDS011
[https://data.sensor.community/airrohr/v1/filter/type=SDS011&country=FR](https://data.sensor.community/airrohr/v1/filter/type=SDS011&country=FR)
- Les capteurs en France
[https://api.luftdaten.info/v1/filter/country=FR](https://api.luftdaten.info/v1/filter/country=FR)

### OpenWeatherMap

Cette API donne le temps des 5 derniers jours. Nécessite une clef d'enregistrement. 60 appel par minutes.

Voir la liste des villes [city.list.json](http://bulk.openweathermap.org/sample/city.list.json.gz) (ouvrir le fichier avec Notepad et faire une recherche pour savoir les villes existantes). Trouver l'identifiant de votre ville (par exemple 3021351 pour Dinard).  
``https://api.openweathermap.org/data/2.5/weather?id=3021351&appid=identifiant_clef_open_weathermap``

### Random User

L'API [random user](https://randomuser.me/) permet de générer des infos pour simuler aléatoirement des coordonnées d'utilisateurs : nom, image, adresse, ... (non existants). Les infos sont renvoyées au format JSON. Un exemple d'appel de cette API est ``https://randomuser.me/api/?results=10``.

### Formspree : envoi de formulaire

[Formspree](https://formspree.io) permet d'envoyer gratuitement des données de formulaires sur son site et de vous notifier par mail de l'envoi d'infos.
La version payante vous permet d'accéder par programmation (API) aux données de vos formulaires.

il faut créer un compte utilisateur sur le site et cette API est ensuite très simple d'utilisation :

- Enregistrer un nouveau formulaire sur Formspree et copier le lien
- Vous pouvez au choix :
  - créer votre formulaire dans votre page et utiliser le lien comme action "submit", méthode "post" de votre formulaire
  - Utiliser le FormButton, un bouton ouvrant le formulaire en popup. Cette solution semble mieux au niveau UX en évitant de créer une page contact. Pour cette solution il faut recopier dans votre page l'extrait de code fourni (snippet) sur le site de formspree. Les champs du formulaire et le style sont définis en json dans le snippet.

### Disqus : envoi de commentaires

[Disqus](https://disqus.com/) fournit un service de commentaires.  La méthode est similaire à Formspree.
Il faut s'enregister créer un nouveau réglage pour un site spécifique (via son url). Un snippet de code est ensuite fourni suivant le type de site où inclure Disqus (Wordpress, drupal, mais aussi manuellement ...).

### Auth0 : l'authentification

Cette API permet de gérer l'authentifications de manière sécurisée à votre site.
Des exemples sont donnés pour utiliser l'API avec des applications JS qui nécessitent cependant du JS côté serveur

### Reddit

Les chaînes Reddit sont accessibles en json en rajoutant json à la suite du sujet.

https://www.reddit.com/r/webdev → https://www.reddit.com/r/webdev.json

https://pix.reddit.com  

### Pays du monde

L'API [REST Countries](http://restcountries.eu/) fournit des informations sur les pays du monde, capitales, monnaie,... 
Voir la [documentation](http://restcountries.eu/#api-endpoints)

Exemple :

```html
https://restcountries.eu/rest/v2/all?fields=name;capital;currencies
```

### ColorTag API documentarion

ColorTag est une API qui permet de détecter les couleurs dans une image. Cette API est gratuite et permet d'utiliser les méthodes ``GET`` et ``POST``, soit pour analyser une image d'après son url soit d'envoyer une image pour analyse.
Voir la [documentation](http://apicloud.me/apis/colortag/docs/).

### API de géolocalisation

OpenStreeMap / Leaflet.js ou GoogleMaps

### Des poèmes

Il existe des API pour récupérer des poèmes :

- [PoetryDB](https://github.com/thundercomb/poetrydb/blob/master/README.md)
- Poemist api : récupération de poèmes aléatoires via l'URL
``https://www.poemist.com/api/v1/randompoems``

### Youtube API

À faire.

## Les API GraphQL

REST est la technologie actuelle la plus utilisées pour les API. La tecnhologie GraphQL est émergeante et permet plus de flexibilité dans la recherche d'information en fournissant un langage de requête pour disposer des données d'une base sous forme d'API. Dans les API REST les requêtes sont préparées par les développeurs de l'API et figées.  

Le principe est donc d'envoyer une requête au format JSON à l'API plutôt qu'une simple url en fonction de l'API. Il arrive aussi qu'un wrapper GraphQL soit créé autour d'une API REST pour rendre l'accès aux données plus souple.

Vous trouverez une [liste d'API graphQL](http://apis.guru/graphql-apis/) que pouvez tester en ligne.

Par exemple, l'[API StarWars](https://github.com/graphql/swapi-graphql) que vous pouvez tester [ici](https://graphql.org/swapi-graphql). Les clefs d'accès aux données sont spécifiées dans un fichier [schema.graphql](https://github.com/graphql/swapi-graphql/blob/master/schema.graphql)

```graphql
 {
  allPeople {
    edges {
      node {
        name
        species {
          id
        }
        gender
        mass
      }
    }
  }
}
```

## Exercices

1. Auto-formez vous à l'exécution de scripts Javascript. Quelques ressources sont par exemple :
   - Les [bases de Javascript sur MDN](https://developer.mozilla.org/fr/docs/Apprendre/JavaScript)
   - Les [bases de javascript  sur w3schools](https://www.w3schools.com/js/default.asp)
   - Une approche par le canvas et le dessin [section learn du site Scratch to Js](https://s2js.com/)
2. Découvrir le format JSON, et son utilisation en Javascript, par exemple sur :
    - [w3schools](https://www.w3schools.com/Jsref/jsref_obj_json.asp)
    - Exemple plus détaillée sur [MDN](https://developer.mozilla.org/fr/docs/Learn/JavaScript/Objects/JSON)
4. Tester l'appel d'une API simple, comme par exemple l'API chuck Norris. Une exemple du code Javascript pour faire l'appel est donné en ressource, fichier ``ex_1_appel_API.html``
5. Implémenter une page appelant des API pour la météo et la pollution de l'air et réaliser une mise en page sympa des infos récupérées (météo & pollution actuelle, prévision à 5 jours).
6. Ajouter un formulaire (champs texte + bouton) pour spécifier une ville ou un emplacement GPS (suivant ce que propose l'API météo utilisée) et mettre à jour les données de la page.

**L'ensemble des tests réalisés doit être mis dans un projet Gitlab** contenant à la racine un ``Readme.md`` d'explication des fichiers et tests réalisés.

### Critères d'évaluation

L'évaluation portera sur les points suivants :

- Présentation des tests réalisés en Javascript.
- Utilisation du format JSON.
- Revue de la documentation et du code sur Gitlab.

### Compétences développées

- Bases d'appel du Javascript (appel d'un script, personnalisation)
- Initiation aux pages web dynamiques
- Découverte et utilisation du format JSON
- Appel d'API Web et formatage de données externes

### Ressources

- Cours en ligne Javascript
- Page d'exemple d'appel d'API
- Une exemple similaire d'application complète, mais plus complexe est donné sur le site de Google Developper, [First PWA](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/). Intéressant à regarder pour l'appel d'API et le formatage des cartes météo

![exemple-1st-pwa-google.png](/cours-giloop/images/exemple-1st-pwa-google.png)
