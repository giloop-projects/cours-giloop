---
title: "Utiliser des templates"
weight: 4
author: [Gilles Gonon]
tags: [javascript, dev, code, js, mustache]
chapter: false
---


## Mustache, un template simple

### Les templates introduction

La différence entre les pages statiques et les pages dynamiques tient au fait que l'information est chargée "dynamiquement" dans les secondes.
Pour cela, au niveau informatique, cela signifie que des infos variables sont injectées dans la page :

- soit au niveau du serveur, en PHP par exemple. Dans ce cas, les informations sont injectées dans le code avant envoi au client.
- soit au niveau du front-end, en Javascript. Dans ce cas les informations sont injectées dans la page durant la navigation. Ces informations sont récupérées via des API et souvent en utilisant des requêtes asynchrones pour ne pas ralentir la navigation.

Une pratique courante pour injecter les données variables dans une page est d'utiliser un langage de **template** (aussi appelé gabarit, modèle, ...).

Pour les pages web, un **template** ressemble à du code HTML contenant des balises d'une syntaxe particulière pour pouvoir y inclure des variables. Il faut ensuite un **moteur de template** auquel on passe le template et les données et qui créé le code.

![Schéma d'utilisation d'un template pour générer le rendu d'une page](/cours-giloop/images/template-mustache.png)

Il existe de nombreux [moteurs et langages de templates](https://en.wikipedia.org/wiki/Comparison_of_web_template_engines) pour les différents langages. Pour le web on retrouve par exemple (liste non exhaustive) :

- [Twig](https://twig.symfony.com/) pour PHP, notamment pour travailler avec le framework Symfony
- [Jinja2](http://jinja.pocoo.org/) ou [Django](https://www.djangoproject.com/) pour Python
- [Mustache](http://mustache.github.io/) pour de nombreux langages notamment pour le JS

Mustache est un langage de template côté client dit *logic-less*, c'est-à-dire qu'il ne définit pas de structure de contrôle explicite (pas de *if*, *for*, ...) et travaille à partir des données suivant leur type. La syntaxe est donc très simple. 

L'exemple ci-dessous montre les principales fonctionnalités du langage, avec un jeu de données simple :

- Afficher une variable. La variable peut être référencée dans l'objet avec la notation pointée
- Afficher une variable contenant des balises HTML : en *échappant* les caractères HTML (par défaut) ou en les intégrants dans le rendu (en utilisant 3 accolades `{{{ variableHTML }}}`)
- Définir une section : On peut accéder au sous-élément d'un objet en déclarant une section, balisée par la syntaxe `{{#maVariable}}` et `{{/maVariable}}`
- Parcourir les élements d'un tableau : il suffit de définir une section avec le nom de la variable (clef) du tableau
- Affichage suivant la valeur d'une variable booléenne, une section est parcourue si la valeur de la variable est définie ou vaut `true`, pour parcourir une section si la variable faut false, utiliser la syntaxe `{^maVariable}}`.

L'objet JSON des données à injecter dans cet exemple (définit dans le script) est :

```json
{ 
    "name" : {"nom":"<b>Gonon</b>", "prenom":"Gilles"},
    "tableau" : ["élément 1", "élément 2", "toto", "titi"],
    "test": true
}
```

Le code de l'exemple est donné ci-dessous :

```html
<div id="injection">
</div>

<script id="template-1" type="x-tmpl-mustache">
    <h1>{{ name.prenom }} {{ name.nom }}</h1>
    
    {{#name}}
    <p>Bonjour, {{ prenom }} {{{ nom }}} 😘</p>
    {{/name}}

    <ul>
        {{#tableau}}
        <li>{{.}}</li>
        {{/tableau}}
    </ul>

    {{#test}}
    Ne s'affiche que si test = true
    {{/test}}

    {{^test}}
    S'affiche quand test = false
    {{/test}}
    
</script>

<script src="https://unpkg.com/mustache@latest"></script>
<script>
    // Création d'une données JSON
    const donnees = { 
        "name" : {"nom":"<b>Gonon</b>", "prenom":"Gilles"},
        "tableau" : ["élément 1", "élément 2", "toto", "titi"],
        "test": true
    };
    
    // Récupération du template Mustache
    var template1 = document.getElementById('template-1').innerHTML;

    var rendered = Mustache.render(template1, donnees);
    document.getElementById('injection').innerHTML = rendered;    
</script>
```
