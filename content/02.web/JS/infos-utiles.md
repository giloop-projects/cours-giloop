---
title: "Infos utiles"
weight: 8
author: [Gilles Gonon]
tags: [javascript, FAQ]
chapter: false
---

## Quelques infos utiles et bonnes pratiques

### Où mettre la balise <script> dans mes pages HTML ? 

Face à cette incontournable question quand apparait le besoin d'optimisé le chargement de ses pages, une très bonne réponse est donnée dans ce [post StackOverflow](https://stackoverflow.com/questions/436411/where-should-i-put-script-tags-in-html-markup).

En résumé, l'état de l'art actuel est de mettre dans l'entête de son code HTML, *<HEAD>*, le chargement des bibliothèques JS en `async` ou `defer` (si l'ordre d'exécution de scripts est important), et d'intialiser / modifier le contenu de vos pages dans vos une fois la page chargée à l'aide d'un callback `ready` : 

HTML : 

```html
<script src="path/to/librairie1.js" async></script>
<script src="path/to/my-script.js" async></script>
```

Javascript : 

```js
// my-script.js
document.addEventListener("DOMContentLoaded", function() { 
    // Cette fonction est executée quand le DOM est prêt
    // c'est-à-dire quand le document a été parsé
    document.getElementById("user-greeting").textContent = "Welcome back, Bart";
});
```

### Détecter un mobile

Si votre souhait est d'adapter le contenu de votre site à la taille de l'écran, ce sont les média queries CSS qui vous seront utiles. Il existe de plus une fonctionnalité Javascript [`window.matchMedia`](https://developer.mozilla.org/fr/docs/Web/API/Window/matchMedia) pour tester dans quel cas de media query on se trouve, par exemple : 

```js
let mql = window.matchMedia('(max-width: 600px)');
```

Si votre souhait est de détecter des fonctionnalités particulière du navigateur, vous pouvez utiliser la [librairie modernizr](https://modernizr.com/)

Remarque : une méthode simple et longtemps utilisée était de détecter une chaîne dans le nom du `navigator.userAgent`. Cette méthode n'est plus recommandée dans les bonnes pratiques. 