---
title: "Migration d'un site Wordpress"
author: "Gilles Gonon"
date: "2020-01-29"
tags: ["wordpress", "hégergement", "site", "web", "migration"]
---

Cette page explique comment migrer un site réalisé avec Wordpress en local vers un hébergeur et réciproquement.

## Migration d'un Wordpress local → hébergeur

### Pré-requis 

- Disposer d'un hébergement web (pour les tests nous utiliserons un hébergeur gratuit 000webhost.com)
- Connaître les **identifiants** du **serveur ftp** de votre hébergeur et de sa **base de données** (souvent les hébergeurs donnent accès via phpMyAdmin à la base de données)
- Avoir un logiciel de transfert FTP, comme [FileZilla](https://www.filezilla-project.org/)

### Étape 1 : préparation des fichiers

En tout premier, il faut faire une copie de l'ensemble du dossier Wordpress du projet local à envoyer en ligne. Par exemple, copier ``C:\Dev\Web\wordpress`` → ``C:\Dev\Web\wordpress-migration``

![fichiers-wordpress.png](/cours-giloop/images/fichiers-wordpress.png)

**Ensuite on ne travaillera plus que sur les fichiers du dossier ``wordpress-migration``.**

### Étape 2 : modification wp-config.php

Modifier en local le fichier `wp-config.php` pour qu'il corresponde aux identifiants de votre base de données **sur l'hébergeur**.

```php
// ** MySQL settings - You can get this info from your web host ** 
/** The name of the database for WordPress */
define('DB_NAME', 'a_changer');

/** MySQL database username */
define('DB_USER', 'a_changer');

/** MySQL database password */
define('DB_PASSWORD', 'a_changer');

/** MySQL hostname */
define('DB_HOST', 'a_changer');
```

**En option** : plus bas, vers la ligne 60, dans le fichier wp-config.php, il est possible d'indiquer un préfixe de table différent, par défaut ``wp_``. Cela implique cependant une vigilance accrue lors de l'export de la base de données car il faudra modifier le fichier SQL d'export pour prendre en compte les nouveaux noms des tables côté hébergeur. 

### Étape 3 : migration de la base de données

En local, à l'aide de [phpMyAdmin](http://localhost/phpmyadmin/), exporter uniquement les tables de Wordpress en SQL. Ce sont les tables qui commencent par le préfixe renseigné dans le fichier wp-config.php original. 
   - Utiliser le Filtre avec le préfixe des tables de votre Wordpress, par exemple ``vv_``

![export-bdd-wordpress-1.png](/cours-giloop/images/export-bdd-wordpress-1.png)

- Choisir le menu Exporter et prendre les choix par défaut :

![export-bdd-wordpress.png](/cours-giloop/images/export-bdd-wordpress.png)

L'export crée un fichier SQL, c'est-à-dire un fichier texte qui contient l'ensemble des commandes SQL nécessaires pour créer les tables de votre site Wordpress et leur contenu. 

A ce stade, il est nécessaire d'éditer le fichier SQL créé pour modifier les éléments suivants : 

- Remplacer les occurrences de l'URL de votre site local par celles de votre hébergement distant. Par exemple ``http://localhost/wordpress`` → ``https://monsite.com/``
- **En option** : modifier les préfixes des tables Wordpress si besoin (si un autre Wordpress existe) en concordance avec le fichier `wp-config.php`

Importer la base sur votre hébergeur avec l'outil fourni dans la gestion de votre espace web, typiquement **phpMyAdmin**.


### Étape 4 : envoi des fichiers

A l'aide d'un client FTP, par exemple FileZilla, envoyer l'ensemble des fichiers du dossier ``wordpress-migration`` vers le dossier où vous voulez avoir Wordpress chez votre hébergeur.


## Récupération depuis hébergeur → local  

Suivre les étapes suivantes : 

1. A l'aide du client FTP **télécharger** tous les fichiers de votre Wordpress en local dans un nouveau dossier. 
   - Modifier en local le fichier `wp-config.php` pour qu'il corresponde aux identifiants de votre base de données locaux ainsi que le préfixe des tables s'il a changé.
 
```php
// ** MySQL settings - You can get this info from your web host ** 
/** The name of the database for WordPress */
define('DB_NAME', 'tade
laktb321');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'ROOT@mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');
```

2. A l'aide de [phpMyAdmin](http://localhost/phpmyadmin/), exporter les tables de Wordpress en SQL
   - Utiliser le Filtre avec le préfixe des tables de votre Wordpress

![export-bdd-wordpress-1.png](/cours-giloop/images/export-bdd-wordpress-1.png)

- Choisir le menu Exporter et prendre les choix par défaut :

![export-bdd-wordpress.png](/cours-giloop/images/export-bdd-wordpress.png)

- À ce stade, il est possible d'éditer le fichier sql créé pour modifier les éléments suivants : 
    - Remplacer les occurrences de l'URL de votre site distant par celles sur votre hébergement local (créer un alias Apache si besoin)
    - Modifier les préfixes des tables Wordpress si besoin (si un autre wordpress existe) en concordance avec le fichier `wp-config.php`
- Importer la base en local dans phpMyAdmin
