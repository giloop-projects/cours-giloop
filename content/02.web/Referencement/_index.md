---
title: Référencement
weight: 10
chapter: false
---


## Référencement Web

Le référencement des pages est une science complexe qui mélange pluiseurs compétences. C'est aussi un domaine en constante évolution, dont les règles sont fixées par Google.

En tant que développeur Web vous devez connaître quelques conseils et bonnes pratiques à respecter au niveau du code.

### Glossaire

- Référencement
- SEO : Search Engine Optimisation
- SMO : Social Media optimization
- SERP : Search engine Result Page
- Robot, Spider, Crawler : agents automatiques qui parcourent les liens du site via l'index
- PageRank

### Fichiers robots.txt

Voir les [explications sur le fichier robots.txt](http://robots-txt.com/)

### Les étapes

- Travailler le Code
- Travailler le contenu : comprendre la [structure d'un article](http://ifsinanterre.free.fr/Documents/groupedoc/Titraille.pdf)
- Obtenir des liens entrants vers votre site
- Enregistrer le site sur [Google My Business](https://www.google.com/business/)

### Les tendances

- Le SEO locale : 40% des recherches mobiles ont un but “local”, et 88% des consommateurs qui effectuent une recherche locale vont avoir une action dans les 24h, visitant ou appelant le résultat de leur requête.
- Google : de *Moteur de recherche* à *moteur de réponse*. Préparer vos sites pour le *0 clic* : Featured Snippets (Extraits optimisés) et les PAA (People Also Ask)
- La réputation en ligne joue un rôle également prédominant, qui va plus loin que le rôle du développeur

### Ressources

- [Google search Console](https://search.google.com/search-console/welcome)
- [Outil de planification de mots clefs](https://ads.google.com/intl/fr_fr/home/tools/keyword-planner/)
- [Appliquer des fonctionnalités de résultats de recherche à votre site](https://support.google.com/webmasters/answer/7358659)
- spider simulator
- [Le balisage sémantique](https://www.lafabriquedunet.fr/seo/articles/mettre-en-place-installer-balisage-semantique-schema-org/)
- [Outil Google d'aide au balisage sémantique](https://www.google.com/webmasters/markup-helper/u/0/)
- [Blog de miss SEO](https://www.miss-seo-girl.com/)