---
title: Altcodes, ÀÉÈÈÇÂœ
weight: 20
date: 20210310
chapter: false
tags: [web, astuces, faq, keyboard, altcodes]
---

## Altcodes : caractères spéciaux au clavier

Sous Windows il est possible de taper des caractères n'existant pas au clavier en utilisant le raccourci clavier `Alt+n°`.

- Accents français sur les majuscules :
  - `alt+0192` : À
  - `alt+0201` : É
  - `alt+0200` : È
  - `alt+0202` : È
  - `alt+0199` : Ç
  - `alt+0194` : Â
  - `alt+0156`: œ

- Exemples des caractères `alt+1 → alt+31` : ☺☻♥♦♣♠•◘○◙♂♀♪♂♫☼►◄↕‼¶§▬↨↑↓→←∟↔▲▼  

- Quelques utilitaires : 
  - `alt+26` : →
  - `alt+184` : ©
  - `alt+169` : ®
  - `alt+190` : ¥
  - `alt+252` : ³
  - `alt+253` : ²
  - `alt+0137` : ‰

La page [Wikipédia des altcodes](https://fr.wikipedia.org/wiki/Alt_codes) contient une table plus complète.
