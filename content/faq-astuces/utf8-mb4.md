---
title: utf8-mb4 😍
weight: 10
#pre: "<b>2. </b>"
chapter: false
tags: [web, astuces, faq, emojis, utf8, unicode, utf8mb4]
---

## Sélection de caractères unicodes utf8-mb

Les caractères ci-dessous peuvent copier-collés. Vous pouvez en trouver d'autres sur internet :

- par exemple sur [utf8icons](https://www.utf8icons.com/).
- Recherche par mot clef sur [emojipedia](https://emojipedia.org/)
- Les documents normatifs sur unicode.org : https://home.unicode.org/emoji/about-emoji/
- https://unicode.org/emoji/charts/index.html
- Si vous voulez que vos emojis soient reconnus, [emojination](http://www.emojination.org/)
- Une visualisation des [émojis utilisés sur Twitter](http://emojitracker.com/) en temps réel

Emoticons / Humains  
😀😁😂😃😄😅😆😇😈👿😉😊😋😌😍😎😏😐😑😒😓😔😕😖😗😘😙😚😛😜😝😞😟😠😡😢😣😤😥😦😧😨😩😪😫😬😭😮😯😰😱😲😳😴😵😶😷😸😹😺😻😼😽😾😿🙀🙁🙂🙃🙄🤐🤑🤒🤓🤔🤕🤖🤗🤠🤡🤢🤣🤤🤥🥰🥱🥳🥴🥵🥶🥺🤧🤨🤩🤪🤫🤬🤭🤮🤯🧐👼💀☠
🙅🙆🙇🙈🙉🙊🙋🤦🙍🙎👤👥👦👧👨👩👪👫👬👭👮👯👰👱👲👳👴👵👶👷👸👹👺👻👽👾💀💁💂💐🤳🤴🤵🤶🤷🧘🧙🧚🧛🧜🧝🧞🧟🦰🦱🦲🦳🦴🦵🦶🦷🦸🦹🦺🦻🦼🦽🦾🦿🧍🧎🧏🧑🧒🧓🧔🧕🧖🧗
🙏🖕🖖🤏 👀👁👂👃👄👅🙌👆👇👈👉👊👋👌👍👎👏👐🤲✊✋✌🤘🤙🤚🤛🤜🤝🤞🤟✍💪💩🧻
👑👒👓👔👕👖👗👘👙👚🩰🩱🩲🩳👛👜👝👞👟👠👡👢👣 🧠🧡🧢🧣🧤🧥🧦🧧🧵🧶🧷🧸🧹🧺🧼🧽🧾
 🩸🩹🩺🥻🥼🥽🥾🥿🦯💉💊💋💌💍💎

💑💒💓💔💕💖💗💘💙💚💛💜💝💞💟🤍🤎🤰🤱🥂🥃🥄💌❣

Signes  
⚡🔥📌💩⛔✅❌❎❓❗❔❕💡💢💣💤💥💨💫🔞💬💭🔍🔎📯⚓🔦🔧🔨🧨🧩🧪🧫🧬🧭🧮🧯🧰🧱🧲🧳🧴✎✏✐⭐🎓🗳️🛡️🏸📝

Agenda, Annonces, planning  
⏳ 📣 ⏰ 📅⌚📅📆⏲️🗃️🎛️🎨
🕐🕑🕒🕓🕔🕕🕖🕗🕘🕙🕚🕛🕜🕝🕞🕟🕠🕡🕢🕣🕤🕥🕦🕧

Nourriture  
🍴🔪 🍕 🍖 🍪🍟🌭🌮🌯🌰🌱🌲🌳🌴🌵🌶🌷🌸🌹🌺🌻🌼🌽🌾🌿🍀🍁🍂🍃🍄🍅🍆🍇🍈🍉🍊🍋🍌🍍🍎🍏🍐🍑🍒🍓🍔🍕🍖🍗🍘🍙🍚🍛🍜🍝🍞🍟🍠🍡🍢🍣  🍤🍥🍦🍧🍨🍩🍪🍫🍬🍭🍮🍯🍰🍱🍲🍿🍤🍥🍦🍧🍨🍩🍪🍫🍬🍭🍮🍯🍰🍱🍲🍳🍴🍵🍶🍷🍸🍹🍺🍻💩🥏🥐🥑🥒🥓🥔🥕🥖🥗🥘🥙🥚🥛🥜🥝🥞🥟🥠🥡🥢🥣🥤🥥🥦🥧🥨🥩🥪🥫🥬🥭🥮🥯🧀🧁🧂🧃🧄🧅🧆🧇🧈🧊

Boissons  
🍶🍷🍸🍹🍺🍻🍼🍽🍾☕🧉

Clefs, privacy  
🔏🔐🔑🔒🔓🔔🔕🔖🔗

Temps
 🌂🌃🌄🌅🌆🌇🌈🌉🌊🌋🌌🌍🌎🌏🌐🌑🌒🌓🌔🌕🌖🌗🌘🌙🌚🌛🌜🌝🌞🌟🌠 🗻🗼🗽🗾🗿 ☔⛅

 Activités  
🎠🎡🎢🎣🎤🎥🎦🎧🎨🎩🎪🎫🎬🎭🎮🎯🎰🎱🎲🎳🎴🎵🎶🎷🎸🎹 🏈🏉🏊🏋🏌🏏🏐🏑🏒🏓 ⛳⛵⛹⛺🎺🎻🎼🎽🎾🎿🏀🏁🏂🏃🏄🏅🏆🏇⚽⚾⛄⚓🔩🔫🔬🔭🔮✨🔱🤸🤹🤺🤻🤼🤽🤾🤿🥀🥁🥅🥇🥈🥉🥊🥋🥌🥍🥎🪀🪁🪂🪐🪑🪒🪓🪔🪕🏴🏹🏺

🎀🎁🎂🎃🎄🎅🎆🎇🎈🎉🎊🎋🎌🎍🎎🎏🎐🎑🎒🎓

 Animaux  
🐾🐀🐁🐂🐃🐄🐅🐆🐇🐈🐉🐊🐋🐌🐍🐎🐏🐐🐑🐒🐓🐔🐕🐖🐗🐘🐙🐚🐛🐜🐝🐞🐟🐠🐡🐢🐣🐤🐥🐦🐧🐨🐩🐪🐫🐬🐭🐮🐯🐰🐱🐲🐳🐴🐵🐶🐷🐸🐹🐺🐻🐼🦀🦁🦂🦃🦄🦅🦆🦇🦈🦉🦊🦋🦌🦍🦎🦏🦐🦑🦒🦓🦔🦕🦖🦗🦘🦙🦚🦛🦜🦝🦞🦟🦠🦡🦢🦥🦦🦧🦨🦩🦪🦮

 Immobilier / lieux  
🏠🏡🏢🏣🏤🏥🏦🏧🏨🏩🏪🏫🏬🏭🏮🏯🏰⛪⛲🕋🕌🕍⛽🏔🏕🏖🏗🏘🏙🏚🏛🏜🏝🏞🏟💈
  
 💠💦💧💮💯💰💱💲💳💴💵💶💷💸💹💺💻💼💽💾💿📀📁📂📃📄📇📈📉📊📋📍📎📏📐📑📒📓📔📕📖📗📘📙📚📛📜📝📞📟📠📡📢📣📤📥📦📧📨📩📪📫📬📭📮📰📱📲📳📴📵📷📸📹📺📻📼📿🔅🔆🔇🔈🔉🔊🔋🔌  💸
📶🔀🔁🔂🔃🔄🔙🔚🔛🔜🔝🔟🔠🔡🔢🔣🔤🔼🔽🎦🔯🕎♿
♈♉♊♋♌♍♎♏♐♑♒♓
🔲🔳🧿🔴🔵🔶🔷🔸🔹🔺🔻⚪⚫🔘⭕🔰

Non triés :
🚀🚁🚂🚃🚄🚅🚆🚇🚈🚉🚊🚋🚌🚍🚎🚏🚐🚑🚒🚓🚔🚕🚖🚗🚘🚙🚚🚛🚜🚝🚞🚟🚠🚡🚢🚣🚤🚥🚦🚧🚨🚩🚪🚫🚬🚭🚮🚯🚰🚱🚲🚳🚴🚵🚶🚷🚸🚹🚺🚻🚼🚽🚾🚿🛀🛁🛂🛃🛄🛅🛌🛐🛑🛒🛕🛫🛬🛴🛵🛶🛷🛸🛹🛺🛡️🚏

Et en prime un petit script js pour explorer les tables de caractères utf8mb4

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <p>Test HTML &#x1F31E; &#x2600;</p>
    <p id="table"></p>
    <script>
    // Voir code ci-dessous
    </script>
</body>
</html>
```

 ```javascript
table = document.getElementById('table');

var charDeb = parseInt('1F000',16); // 1048576
var charFin = parseInt('1FA96',16); // 2097151
var tabChars = [];
var br = 0;
for (a = charDeb; a<charFin; a++) {
    tabChars.push('&#x' + a.toString(16)+';');
    if (br++ > 70) {
        tabChars.push(' <br/> ');
        br = 0;
    }
}

table.innerHTML = `Caractères de ${charDeb}=${charDeb.toString(16)} à ${charFin}=${charFin.toString(16)} <br>`
                + tabChars.join("");
```
