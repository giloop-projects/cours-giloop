---
title: Chocolatey
weight: 20
#pre: "<b>2. </b>"
chapter: false
tags: [web, astuces, faq, chocolatey, choco]
---


## Chocolatey : mises à jour

Chocolatey permet d'automatiser l'installation de logiciels sous windows. Il permet aussi de gérer très simplement les mises à jour.

Voir les paquets installés :  ``choco list -lo``

```bash
PS C:\WINDOWS\system32> choco list -lo
Chocolatey v0.10.15
chocolatey 0.10.15
dart-sdk 2.7.0
hugo 0.68.3
keepassxc 2.5.3
make 4.3
sass 1.25.0
yarn 1.22.0
7 packages installed.
```

Pour vérifier les versions des logiciels installés et ceux qui disposent de mises à jour, sans les appliquer (option `--noop` pour _no operation_) :

```bash
choco upgrade all --noop`
```

Pour mettre à jour chocolatey lui-même :

```bash
choco upgrade chocolatey
```

Pour tout mettre à jour :

```bash
choco upgrade all
```

On peut également remplacer `all` par le nom de n'importe quel paquet (nom affiché dans la liste avant le numéro de version).
