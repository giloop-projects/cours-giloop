---
title: Visual Studio Code
weight: 20
#pre: "<b>2. </b>"
chapter: false
tags: [web, astuces, faq, vscode, emmet]
---

### Emmet

Emmet est cette extension qui crée automatiquement une balise quand vous tapez son nom dans une page HTML.  
💡 On parle de **snippets** de code pour nommer des bouts de codes qui reviennent souvent.

Par exemple si vous tapez dans l'éditeur `p`, cela génère une balise complète `<p></p>`, 🧐.

Les possibilités sont énormes pour accélérer vos manière de créer des pages 😂.

Tapez par exemple dans l'éditeur, la ligne suivante. Notez qu'au fur et à mesure que vous tapez, une info-bulle vous donne le rendu en code HTML :

```html
ul>li{item $}*5
```

![Rendu de l'exemple ci-dessus](/cours-giloop/images/emmet-ex-5-li.png)

{{% notice warning %}}
🎓 Je vous recommande vivement d'apprendre par :heart: [la doc emmet des abréviations](https://docs.emmet.io/abbreviations/syntax/)
{{% /notice %}}

### Emmet en Markdown

Si vous utilisez un générateur de site statique, comme Hugo, il est intéressant de mettre du HTML dans les fichiers markdown. Pour accélérer la rédaction, vous pouvez activer Emmet dans les fichiers Markdown : 

- Aller dans les préférences de VS code (Menu File → Preferences ...)
- Chercher Emmet dans la barre de recherche
- Éditer le fichier `settings.json` (lien dans les résultats de recherche) en ajoutant :

```json
  "emmet.excludeLanguages": [],
  "emmet.includeLanguages": {
      "markdown": "html"
  },
```

Une autre manière d'accéder au fichier `settings.json` est d'utiliser la `Command pallet ...` (raccourci Ctrl+Shit+P)

![Accès à la palette de commandes de VScode](/cours-giloop/images/astuce-emmet-in-md-1.png)

et de lancer la commande `Preferences: Open settings (JSON)`

![Accès au fichier settings.json](/cours-giloop/images/astuce-emmet-in-md-2.png)

### Recherche/remplacement expression régulière VS code

La capture d'écran ci-dessous montre comment utiliser les expressions régulières dans la recherche / remplacement. Cela devient un outil très puissant. Les expressions régulières permettent de rechercher des _motifs_ ou _patterns_ dans des chaînes de caractères plutôt que des mots fixes. Voir les principes de bases sur [la page Wikipédia des regexp](https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re#Principes_de_base). On peut utiliser de manière basique :

- des caractères, `toto`, `1234`, ou des classes de caractères `:
  - \d` pour les chiffres de 0 à 9,
  - `.` pour tout caractère,
  - `\s` pour les espaces
  - `\w` pour les caractères alphanumériques `[A-Za-z0-9_]`
  - `\` est le caractère d'échappement qui permet de définir les caractères sépciaux dans un motif, par exemple `\(` permet de rechercher une parenthèse dans un motif car elle normalement utilisée pour entourer un groupe de caractère.
- des opérateurs pour former des groupes ou combiner les éléments :
  - `[]` : un des caractères entre les crochets `[abc]` ou encore `[a-z]` pour les caractères minuscules
  - `[^]`: un des caractères qui n'est pas entre les crochets
  - `$` : en début de chaîne
  - `^` : en fin de chaîne
  - `()` : rassemble un groupe de caractères ou de motifs. CEla permet aussi de rappeler l'expression identifier pour un remplacement
  - `|` : OU, l'un ou l'autre `(titi|toto)`
- des quantificateurs pour indiquer le nombre de fois que l'on recherche chaque groupe :
  - `?` : 0 ou 1 fois,
  - `*` : 0 ou plusieurs fois
  - `+` : 1 ou plusieurs fois

Dans l'exemple ci-dessous : la recherche porte sur un motif définissant un lien vers une image en markdown, dont la syntaxe est `![description avec n'importe quel caractère](chemin/vers/l'image)`, ce qui donne le motif `!\[(.)*\](`.

![regexp-replace-vscode.png](/cours-giloop/images/regexp-replace-vscode.png)

{{% notice tip %}}
Les expressions régulières ne sont pas toujours intuitives, il faut tester, encore et encore, et chercher des infos sur internet.
{{% /notice %}}

### Pour aller plus loin

Les expressions régulières peuvent être beaucoup plus complexes, elles forment un véritable langage de programmation, 
avec des conditionnelles, des indicateurs de position, et d'autres [fonctions avancées](https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re#Fonctions_avanc%C3%A9es).
