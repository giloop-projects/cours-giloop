---
title: FAQ - Astuces
weight: 50
#pre: "<b>2. </b>"
chapter: true
tags: [web, wamp, astuces, faq, clavier, raccourcis, utf8, unicode, utf8mb4]
---

### Partie Bonus

# FAQ - Astuces diverses

Cette section n'a ni queue ni tête mais rassemble des infos qui peuvent être précieuses quand vous manquez d'inspiration.
