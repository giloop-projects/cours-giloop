---
title: Réglages Wamp/Xampp
weight: 20
#pre: "<b>2. </b>"
chapter: false
tags: [web, astuces, faq, wamp, xampp, config]
---

## Réglages de WAMP / XAMPP

### Créer des "alias" vers vos dossiers de développement


Wamp rend accessible en localhost les dossiers situés dans le dossier ``www``. Il est aussi possible de créer des ``alias`` ou raccourcis vers vos dossiers de développement situés sur votre disque dur (par exemple dans C:/dev/Web/...).

Les alias sont définis comme des directives du serveur (Apache). 

{{% notice note %}}
Cette étape est importante car en cas de suppression de WAMP de votre ordinateur, vous séparez vos dossiers de travail des dossiers de l'application.
{{% /notice %}}

Pour cela, aller dans le menu de Wamp → Apache → Répertoires Alias → Créer un Alias

![alais-apache-1.png](/cours-giloop/images/alais-apache-1.png)

![Créer un Alias apache depuis WAMP 2/2](/cours-giloop/images/apache-alias-2.png)

Définir alors dans la console qui s'ouvre :

- le nom de l'alias pour y accéder depuis ``localhost`` : adresse Web donc pas d'espace, pas d'accent ou de caractères spéciaux
- le chemin vers le dossier sur votre disque dur

![Exemple de saisie d'un alias Apache dans la console](/cours-giloop/images/exemple-alias-apache.png)

Sous XAMPP, il faut éditer le fichier httpd-xampp.conf (accessible depuis le bouton [config] du serveur), par exemple : 

```xml
Alias /wordpress/ "M:/IMTS/TP/wordpress/"
<Directory "M:/IMTS/TP/wordpress">
      Options Indexes FollowSymLinks MultiViews ExecCGI
      AllowOverride All
      Require all granted
</Directory>
```

### Définir une page en cas d'erreur 404

Apache permet de configurer la page a appeler en cas d'erreur http 404 (page not found). Pour cela modifier le fichier ``httpd.conf``.

![modif-httpd-conf-404.png](/cours-giloop/images/modif-httpd-conf-404.png)

### MySQL : définir un mot de passe pour le compte "root"

Une bonne pratique est de définir un mot de passe pour le compte root de mysql. Pour cela, il faut ouvrir la console MySQL , menu WAMP → MySQL → Console MySQL

![Ouvrir la console MySQL avec WAMP](/cours-giloop/images/Wamp-MySQL-console.png)

Puis taper les commandes suivantes :  

```sql
SET PASSWORD for 'root'@'localhost' = password('UnMot_2_P@sse_CompliquE');
FLUSH PRIVILEGES;
exit
```

Sous XAMPP, lancer une console en cliquant sur le bouton [Shell] du control panel, puis en vous loguant dans le client mysql en ligne de commande : 

```shell
mysql -u root -p
```

Vous pouvez alors taper les commandes ci-dessus. Il faudra ensuite modifier le fichier de configuration de PhpMyAdmin pour vous connecter, fichier `C:\xampp\phpMyAdmin\config.inc.php` : 

```php
/* Authentication type and info */
$cfg['Servers'][$i]['auth_type'] = 'config';
$cfg['Servers'][$i]['user'] = 'root';
$cfg['Servers'][$i]['password'] = 'UnMot_2_P@sse_CompliquE';
```

### Envoyer des mails depuis PHP (XAMPP)

XAMPP contient l'utilitaire [sendmail.exe](http://www.sendmail.com/sm/open_source/) qui permet d'envoyer des mails. Il reste donc à configurer **2 fichiers .ini**.

#### Fichier C:\xampp\sendmail\sendmail.ini 

Renseigner les variables `smtp_server` et `smtp_port` selon votre fournisseur d'accès, comme pour votre client de mail. Par exemple : 

```ini
smtp_server=smtp.orange.fr
smtp_port=465
```

Pour utiliser GMail, il faut en plus renseigner identifiant/mot de passe de votre compte, dans les champs suivants :

```ini
auth_username=mon.mail@gmail.com
auth_password=mon_MOT_2_pass!!

force_sender=mon.mail@gmail.com
```

#### Fichier C:\xampp\php\php.ini

Les réglages sont presque les mêmes, chercher et renseigner les variables `smtp_server` et `smtp_port` avec les mêmes valeurs. Renseigner en plus le chemin vers l'utilitaire `sendmail.exe`, par exemple : 

```ini
sendmail_path=C:\xampp\sendmail\sendmail.exe
```

#### Test

Redémarrez le serveur Apache pour que le php.ini soit rechargé. 

Créez un fichier PHP testmail.php qui envoie un mail : 
```php
<?php
// Arguments :
// mail(email_destinataire, titre_mail, contenu_mail,"From: email_expéditeur");
mail("mon.mail@free.fr","Test sendmail","Un beau message","From: gil.69@free.fr");
?>
```

En cas de problème, cela vient de vos paramètres de configuration SMTP, essayer d'autres réglages en fonction de votre opérateur (faites une recherche sur internet). 

Si vous travaillez en localhost, il est important de bien paramétrer l'envoi avec une adresse cohérente au SMTP. Sinon certaines messagerie classerons automatiquement les messages que vous envoyez en **spam**. Par exemple sous Gmail : 

![Problème cohérence entre adresse From et SMTP sur Gmail](sendmail-mauvais-smtp.png)

Vous pouvez également visiter ces pages : 

- [Envoyer des emails en localhost avec XAMPP (Windows)](https://blog.pierre-roels.com/2012/10/envoyer-des-emails-en-localhost-avec-xampp-windows/)
- [How to configure XAMPP to send mail from localhost using PHP ?](https://www.geeksforgeeks.org/how-to-configure-xampp-to-send-mail-from-localhost-using-php/)

Enfin pour ce qui est d'envoyer des newsletter par mail, voici quelques conseils : 

- Inspirez vous du code source des mails de newsletter que vous recevez, il est toujours possible d'afficher la source d'un mail et d'en récupérer la partie `Content-Type: text/html`
- La mise en page des mails utilise des tables html (usage intensif...)
- La largeur typique d'une newsletter est entre 600 et 640px
- Prévoyez toujours une alternative en texte brute à votre message (les 2 peuvent être envoyés en même temps, ce qu'on appelle **MIME**).
- Pour vos premiers mails, testez le rendu de votre mail sur différents clients mail (thunderbird, K9 mail, Gmail, zimbra, orange, ...) sur mobile et sur PC.
- Quelques modèles : 
  - [email html templates](https://github.com/ColorlibHQ/email-templates), 
  - [creating a semplie ersponsive html email](https://codepen.io/tutsplus/pen/BaBQgGZ)

