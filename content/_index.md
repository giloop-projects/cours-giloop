---
title: "Ressources de cours Gilles Gonon"
---

# Les cours de Gilles Gonon

## Introduction

Ce site a été initié en 2020 pour rassembler les notes de cours données à l' [Institut Marie-Thérèse Solacroup](http://institutsolacroup.com/) pour les formations _Développeur Web_ et _Tremplin vers le numérique_. 

Ces notes restent en évolution aléatoires au hasard de mes envies et de mes découvertes.

## Documentation du site

{{% notice note %}}
Le site est automatiquement publié et hébergé par [Gitlab](https://gitlab.com/). Plus d'infos sur le [déploiement de site Hugo avec Gitlab](https://docs.gitlab.com/ee/user/project/pages/)(En anglais)
{{% /notice %}}

Cette documentation est un site statique générée avec Hugo avec une simple commande : `hugo`.

Ces notes sont en libre accès. Pour les récupérer et modifier, voir cette page d'[explications](utilisation).
