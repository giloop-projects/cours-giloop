# Notes de cours informatique 

Ce [dépot](https://gitlab.com/giloop-projects/cours-giloop) contient les sources du site https://giloop-projects.gitlab.io/cours-giloop/

Le site est publié avec Hugo et utilise le thème Hugo-learn. 

Contenu libre de droit pour (ré)utilisation modification. N'hésitez pas à partager si elles vous sont utiles. 

[Giloop](http://gilles.gonon.free.fr)